module.exports = {
  content: [
    "./pages/**/*.{js,jsx,ts,tsx}",
    "./components/**/*.{js,jsx,ts,tsx}",
    "./node_modules/shamrock-ux/lib/components/**/*.js"
  ],
  safelist: [
    /data-theme/
  ],
  theme: {
    extend: {},
  },
  presets: [require("shamrock-ux/lib/tailwind")()],
};