import { ref, remove, set } from "firebase/database";
import React, { useCallback, useMemo, useState } from "react";
import { Button, InputBox } from "shamrock-ux";
import { database } from "./FirebaseProvider";
import { useSessionContent } from "./SessionContext";

export default function SessionContentPanel({ id }) {
    const sessionContent = useSessionContent();

    //Import session content info into editable states
    const [editingName, setEditingName] = useState(sessionContent?.name ?? "");
    const [editingContent, setEditingContent] = useState(sessionContent?.content ?? "");
    const [editingStart, setEditingStart] = useState(sessionContent?.scheduledStart ?? "");
    const [editingDownload, setEditingDownload] = useState(sessionContent?.download ?? "");

    //Reset changes to sessionContent
    const discardChanges = useCallback(() => {
        setEditingName(sessionContent?.name ?? "");
        setEditingContent(sessionContent?.content ?? "");
        setEditingStart(sessionContent?.scheduledStart ?? "");
        setEditingDownload(sessionContent?.download ?? "");
    }, [sessionContent]);

    //Format the JavaScript date for the HTML datetime-local input
    const dateTimeLocalStart = useMemo(() => {
        if (!editingStart) return "";
        const d = new Date(editingStart);
        d.setMinutes(d.getMinutes() - d.getTimezoneOffset());
        return d.toISOString().slice(0,16);
    }, [editingStart]);

    const saveContent = useCallback(() => {
        if (sessionContent?.name !== editingName) {
            //Update name
            if (!editingName) {
                console.error("You must set a session name");
            } else {
                set(ref(database, `/session/${id}/info/name`), editingName);
            }
        }

        if (sessionContent?.content !== editingContent) {
            //Update content
            if (editingContent) {
                set(ref(database, `/session/${id}/info/content`), editingContent);
            } else {
                remove(ref(database, `/session/${id}/info/content`));
            }
        }

        if (sessionContent?.scheduledStart !== editingStart) {
            //Update scheduled start
            if (editingStart) {
                set(ref(database, `/session/${id}/info/scheduledStart`), editingStart);
            } else {
                remove(ref(database, `/session/${id}/info/scheduledStart`));
            }
        }

        if (sessionContent?.download !== editingDownload) {
            //Update download URL
            if (editingDownload) {
                set(ref(database, `/session/${id}/info/download`), editingDownload);
            } else {
                remove(ref(database, `/session/${id}/info/download`));
            }
        }
    }, [sessionContent, editingName, editingContent, editingStart, editingDownload]);

    return (
        <div className="py-20">
            <div>
                <h1 className="text-type-0 text-4xl font-semibold">Session Content</h1>
                <h2 className="mt-4 text-type-20 text-lg">Defining your session's content lets people know what you're watching and when</h2>
            </div>
            <br />
            <div className="flex flex-col xl:flex-row mt-12">
                <div className="flex-1 space-y-12 xl:mr-12">
                    <div>
                        <p className="text-type-10 text-lg mb-4">Session name</p>
                        <InputBox value={editingName} onChange={e => setEditingName(e.target.value)} icon="text_fields" placeholder="Name" width={500} />
                    </div>
                    <div>
                        <p className="text-type-10 text-lg mb-4">Content name</p>
                        <InputBox value={editingContent} onChange={e => setEditingContent(e.target.value)} icon="movie" placeholder="Name" width={500} />
                    </div>
                </div>
                <div className="flex-1 space-y-12 mt-12 xl:mt-0">
                    <div>
                        <p className="text-type-10 text-lg mb-2">Session start time</p>
                        <p className="text-type-30 text-sm mb-4">Let members know when to join the session by setting a start time</p>
                        <div className="space-y-2">
                            <InputBox value={dateTimeLocalStart} onChange={e => setEditingStart(new Date(e.target.value).getTime())} type="datetime-local" width={500} />
                            <Button onClick={() => setEditingStart("")} icon="clear" size="small" style="outline">Clear</Button>
                        </div>
                    </div>
                    <div>
                        <p className="text-type-10 text-lg mb-2">Content download URL</p>
                        <p className="text-type-30 text-sm mb-4">If a member doesn't have the file you're watching, VLC Sync can direct them to the URL you specify below to download the file</p>
                        <InputBox value={editingDownload} onChange={e => setEditingDownload(e.target.value)} icon="insert_link" placeholder="Download URL" width={500} />
                    </div>
                </div>
            </div>
            <div className="mt-12 flex flex-row items-center space-x-2">
                <Button onClick={saveContent} icon="save">Save</Button>
                <Button onClick={discardChanges} style="outline" icon="close">Discard changes</Button>
            </div>
        </div>
    );
}