import { Modal, Navbar, useTheme } from 'shamrock-ux';
import { useRouter } from 'next/router';
import { useUser, useUsernames } from '../components/FirebaseProvider';
import React, { createContext, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { signOut as signOutFirebase } from 'firebase/auth';
import { auth } from '../components/FirebaseProvider';
import { useSessionContent, useSessionPerms } from './SessionContext';
import { useJoinedSession } from './JoinedSessionRoot';

const globalModalContext = createContext(null);

export function useSetGlobalModal() {
    return useContext(globalModalContext);
}

function AppRoot({ Component, pageProps }) {
    const router = useRouter();
    const user = useUser();

    const loading = user === undefined;

    const myUID = useMemo(() => user?.uid ? [user?.uid] : [], [user?.uid]);
    const usernames = useUsernames(myUID);

    const theme = useTheme();
    useEffect(() => {
        if (!window?.redNode?.titleBarTheme) return;

        //Update the title bar theme
        window.redNode.titleBarTheme({
            color: theme.dark ? theme.ui[10] : theme.ui[0],
            symbolColor: theme.type[0],
            height: 56
        });
    }, [theme]);

    useEffect(() => {
        if (loading) return;

        const needUsername = !user?.isAnonymous && (usernames[user?.uid] === null);

        if (user && needUsername) {
            router.push("/choose-username");
        } else if (user && !needUsername && ((router.pathname === "/login") || (router.pathname === "/choose-username"))) {
            router.push("/");
        } else if (!user && (router.pathname !== "/login")) {
            router.push("/login");
        }
    }, [user, router.pathname, loading, usernames]);


    const [globalModal, setGlobalModal] = useState(null);
    const joinedSession = useJoinedSession();
    const sessionContent = useSessionContent();
    const sessionPerms = useSessionPerms();

    const [isWebUI, setIsWebUI] = useState(false);
    useEffect(() => setIsWebUI(!window.redNode), []);

    const downloadApp = useCallback(() => {
        const isWindows = window.navigator.platform?.includes("Win");
        if (isWindows) {
            window.location.href = "https://red-server.jottocraft.com/update/latest/exe";
        } else {
            setGlobalModal({
                title: "Unsupported platform",
                icon: "warning",
                message: "Currently, VLC Sync only supports Windows devices. You can continue to use the web interface on this device, however, you will not be able to sync playback."
            });
        }
    }, []);

    const navItems = useMemo(() => {
        let appItems = [];
        if ((router.pathname !== "/login") && (router.pathname !== "/choose-username")) {
            const sessionsActive = router.pathname.startsWith("/sessions");
            let sessionItem = { type: "page", name: "Sessions", icon: sessionsActive ? "folder_open" : "folder", onClick: () => router.push("/sessions"), active: sessionsActive };
            if (joinedSession?.id) {
                sessionItem = { type: "page", name: sessionContent?.name, icon: sessionPerms?.private ? "lock" : "public", onClick: () => router.push(`/sessions/${joinedSession.id}`), active: router.pathname.startsWith("/sessions") };
            }

            appItems = [
                sessionItem,
                { type: "page", name: "Settings", icon: "settings", onClick: () => router.push("/settings"), active: router.pathname === "/settings" }
            ];
        }

        let appDownloadItems = [];
        if (isWebUI) {
            if (appItems.length) appDownloadItems.push({ type: "divider" });
            appDownloadItems.push({ type: "page", name: "Download the app", icon: "file_download", onClick: downloadApp, active: false });
        }

        return [
            ...appItems,
            ...appDownloadItems
        ];
    }, [joinedSession?.id, sessionContent?.name, sessionPerms?.private, router, isWebUI, downloadApp]);

    const signOut = useCallback(() => {
        //Exit session first
        joinedSession.update(null);

        signOutFirebase(auth);
    }, [auth]);

    return (
        <>
            {globalModal ? (
                <Modal onClose={() => setGlobalModal(null)} title={globalModal?.title} icon={globalModal?.icon ?? "notifications"}>
                    <p>{globalModal?.message}</p>
                </Modal>
            ) : null}
            <globalModalContext.Provider value={setGlobalModal}>
                {loading ? (
                    <p className="text-3xl text-center py-40">VLC Sync is loading...</p>
                ) : (
                    <Navbar title="VLC Sync" logo="/app/icon.svg" items={navItems}
                        action={user ? {
                            type: "account",
                            name: user?.isAnonymous ? "Anonymous" : usernames[user?.uid] ?? user?.email,
                            dropdown: [
                                { name: "Sign out", icon: "logout", onClick: signOut }
                            ]
                        } : undefined}>
                        <Component {...pageProps} />
                    </Navbar>
                )}
            </globalModalContext.Provider>
        </>
    );
}

export default AppRoot;