import React, { useMemo, useContext } from "react";
import { useUser } from "../FirebaseProvider";
import { usePlaybackState } from "../PlaybackContext";
import { useSessionMembers, useSessionState } from "../SessionContext";
import FileSync from "./FileSync";
import PlaybackSync from "./PlaybackSync";
import TrackSync from "./TrackSync";
import SyncStatus, { syncStatusContext } from "./SyncStatus";

export function useSyncStatus() {
    return useContext(syncStatusContext);
}

export default function SyncContext({ id, children }) {
    const user = useUser();
    const sessionState = useSessionState();
    const members = useSessionMembers();
    const isHost = useMemo(() => user?.uid === sessionState?.host, [user?.uid, sessionState?.host]);
    const hostIsOnline = useMemo(() => members?.[sessionState?.host], [members, sessionState?.host]);

    //Bypass sync layers if we're on the web
    if (!window.redNode) return (
        <SyncStatus id={id} isHost={isHost} hostIsOnline={hostIsOnline}>
            {children}
        </SyncStatus>
    );

    return (
        <FileSync id={id} isHost={isHost}>
            <PlaybackSync id={id} isHost={isHost}>
                <TrackSync id={id} isHost={isHost}>
                    <SyncStatus id={id} isHost={isHost} hostIsOnline={hostIsOnline}>
                        {children}
                    </SyncStatus>
                </TrackSync>
            </PlaybackSync>
        </FileSync>
    );
}