import React, { useEffect } from "react";
import { ref, get, set } from "firebase/database";

import { database } from "../FirebaseProvider";
import { usePlaybackState } from "../PlaybackContext";
import { useSessionState } from "../SessionContext";
import { usePreferences } from "../Preferences";

function getActiveTrack(tracks) {
    if (!tracks) return undefined;
    return tracks.find(t => t.active)?.item;
}

export default function TrackSync({ id, isHost, children }) {
    const playbackState = usePlaybackState();
    const sessionState = useSessionState();
    const { trackSync } = usePreferences();

    //Sync video track
    useEffect(() => {
        const localVideoTrack = getActiveTrack(playbackState?.information?.tracks?.video);
        if (!localVideoTrack || !trackSync) return;
        if (localVideoTrack === sessionState?.videoTrack) return;

        if (isHost) {
            //Update Firebase track to match local
            set(ref(database, `/session/${id}/status/videoTrack`), localVideoTrack);
        } else {
            //Update local track to match Firebase
            const localTrackID = playbackState.information.tracks.video.find(t => t.item === sessionState?.videoTrack)?.val;
            window.redNode.setVLCTrack("video", localTrackID);
        }
    }, [id, isHost, playbackState?.information?.tracks?.video, sessionState?.videoTrack, trackSync]);

    //Sync audio track
    useEffect(() => {
        const localAudioTrack = getActiveTrack(playbackState?.information?.tracks?.audio);
        if (!localAudioTrack || !trackSync) return;
        if (localAudioTrack === sessionState?.audioTrack) return;

        if (isHost) {
            //Update Firebase track to match local
            set(ref(database, `/session/${id}/status/audioTrack`), localAudioTrack);
        } else {
            //Update local track to match Firebase
            const localTrackID = playbackState.information.tracks.audio.find(t => t.item === sessionState?.audioTrack)?.val;
            window.redNode.setVLCTrack("audio", localTrackID);
        }
    }, [id, isHost, playbackState?.information?.tracks?.audio, sessionState?.audioTrack, trackSync]);


    //Sync subtitle track
    useEffect(() => {
        const localSpuTrack = getActiveTrack(playbackState?.information?.tracks?.spu);
        if (!localSpuTrack || !trackSync) return;
        if (localSpuTrack === sessionState?.spuTrack) return;

        if (isHost) {
            //Update Firebase track to match local
            set(ref(database, `/session/${id}/status/spuTrack`), localSpuTrack);
        } else {
            //Update local track to match Firebase
            const localTrackID = playbackState.information.tracks.spu.find(t => t.item === sessionState?.spuTrack)?.val;
            window.redNode.setVLCTrack("spu", localTrackID);
        }
    }, [id, isHost, playbackState?.information?.tracks?.spu, sessionState?.spuTrack, trackSync]);

    return children;
}