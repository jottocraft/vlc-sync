import React, { createContext, useCallback, useEffect, useState } from "react";
import { set, ref, remove } from "firebase/database";
import { usePlaybackState } from "../PlaybackContext";
import { useSessionState } from "../SessionContext";
import { database } from "../FirebaseProvider";

export const fileSyncContext = createContext({ isSynced: null, tryAgain: null });

export default function FileSync({ id, isHost, children }) {
    const playbackState = usePlaybackState();
    const sessionState = useSessionState();

    const [fileIsSynced, setFileIsSynced] = useState(null);

    //Sync host filename
    useEffect(() => {
        if (!isHost) return;

        //We must have a playback state from VLC, be the host, and have a mismatch between VLC and Firebase state
        if (!playbackState || (playbackState?.information?.meta?.filename === sessionState?.video)) return;

        if (playbackState?.information?.meta?.filename) {
            //Update playback video
            set(ref(database, `/session/${id}/status/video`), playbackState.information.meta.filename);
        } else {
            //Remove playback video
            remove(ref(database, `/session/${id}/status/video`));
        }
    }, [id, isHost, !playbackState, playbackState?.information?.meta?.filename, sessionState?.video]);

    //Sync client filename
    const syncFilename = useCallback(() => {
        if (isHost || !playbackState) return;

        if (!sessionState?.video) {
            //Stop video if there is one playing
            if (playbackState?.information?.meta?.filename) {
                window.redNode.stopVLC();
            }
            
            return setFileIsSynced(null);
        }

        if (playbackState?.information?.meta?.filename !== sessionState?.video) {
            console.log("Opening file in VLC....", sessionState?.video);

            //Attempt to open the file in VLC
            window.redNode.openVLCFile(sessionState?.video).then(found => {
                setFileIsSynced(found);
            });
        } else {
            setFileIsSynced(true);
        }
    }, [isHost, !playbackState, sessionState?.video, playbackState?.information?.meta?.filename]);

    //Sync client filename on change
    useEffect(() => {
        if (isHost) return;
        syncFilename();
    }, [isHost, syncFilename]);

    return (
        <fileSyncContext.Provider value={{ isSynced: fileIsSynced, tryAgain: syncFilename }}>
            {children}
        </fileSyncContext.Provider>
    );
}