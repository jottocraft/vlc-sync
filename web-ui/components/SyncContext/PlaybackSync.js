import React, { createContext, useRef, useEffect, useState } from "react";
import { set, ref } from "firebase/database";
import { captureException } from "@sentry/electron/renderer";
import { usePlaybackRef, usePlaybackState } from "../PlaybackContext";
import { useSessionState } from "../SessionContext";
import { database } from "../FirebaseProvider";
import { calcCurrentPlaybackTime } from "../PlaybackContext";

export const playbackAccuracyContext = createContext(0);

//Calculate the offset from an array of round trip latency measurements
function getOffset(roundTripMeasurements) {
    if (!roundTripMeasurements.length) return 0;
    const latency = roundTripMeasurements.reduce((a, b) => a + b) / roundTripMeasurements.length / 2 / 1000;
    console.log("host offset", latency);
    return latency;
}

export default function PlaybackSync({ id, isHost, children }) {
    const playbackState = usePlaybackState();
    const playbackRef = usePlaybackRef();
    const sessionState = useSessionState();

    //Track the playback sync state
    //0 = off; 1 = out of sync; 2 = large offset (<1s); 3 = small offset (<0.5s); 4 = basically no offset (<0.25s);
    const [playbackSyncAccuracy, setPlaybackSyncAccuracy] = useState(0);

    //Locks VLC in the pause state
    //For frame-perfect pausing, we need to consistency update the VLC pause outside the normal WebSocket message interval
    const [vlcPauseLock, setVLCPauseLock] = useState(false);

    //Create conductor socket
    const activeWS = useRef(null);
    const roundTrip = useRef([]);
    const wsNumErrors = useRef(0); //number of consecutive errors
    const [clientOffset, setClientOffset] = useState(null);
    useEffect(() => {
        //No need for a websocket if we are a client with no conductor
        if (!isHost && !sessionState?.conductor) return;

        //No need for a websocket if VLC is not open
        if (!playbackState) return;

        console.log("WS Using playback ref", playbackRef);

        //Set sync status to errored if there aren't any messages
        let errorTimeout;
        function resetErrorTimeout() {
            if (errorTimeout !== undefined) clearTimeout(errorTimeout);

            //Wait for 5 consecutive failures before reporting an error
            errorTimeout = setTimeout(() => {
                setPlaybackSyncAccuracy(1);
            }, 5000);
        }

        let ws, wsID, wsShouldBeOpen = true;
        function openWebSocket() {
            //Create a new socket
            if (isHost) {
                ws = new WebSocket("wss://red-server.jottocraft.com/conductor/new");
                wsID = "new";
            } else {
                ws = new WebSocket(`wss://red-server.jottocraft.com/conductor/${sessionState?.conductor}/`);
                wsID = sessionState?.conductor;
            }

            //Only handle closing once
            ws.hasHandledClose = false;

            //Connection opened
            ws.addEventListener('open', function (event) {
                wsNumErrors.current = 0;

                //Only set to 1 accuracy once the socket is actually open
                setPlaybackSyncAccuracy(1);

                //Request conductor ID if host
                if (isHost) ws.send(JSON.stringify({ type: "whoami" }));
            });

            //Listen for messages
            ws.addEventListener('message', function (event) {
                const receivedAt = new Date().getTime();
                const data = JSON.parse(event.data);

                switch (data.type) {
                    case "clientTime": {
                        if (isHost) return;

                        //We don't actually know if it's OK yet but want to get a message ASAP for low latency
                        ws.send(JSON.stringify({ type: "OK", nonceTS: data.nonceTS }));

                        //Set VLC to play; release pause lock if set
                        setVLCPauseLock(false);
                        if (playbackRef.current?.state !== "playing") {
                            window.redNode.playVLC();
                        }

                        //Set VLC to play from current timestamp
                        const currentTime = calcCurrentPlaybackTime(playbackRef.current);
                        const offset = currentTime - data.time;
                        setClientOffset(offset);

                        const offsetAbs = Math.abs(offset);
                        const lowLatency = window.localStorage.getItem("lowLatency") !== "false";
                        if (offsetAbs > (lowLatency ? 0.5 : 1)) {
                            console.log("[SKIPPING]", offset, currentTime, "->", data.time);
                            window.redNode.seekVLCTo({ time: data.time, at: receivedAt });
                        }

                        setPlaybackSyncAccuracy(offsetAbs < 0.25 ? 4 : offsetAbs < 0.5 ? 3 : offsetAbs < 1 ? 2 : 1);

                        resetErrorTimeout();

                        return;
                    }
                    case "clientPause": {
                        if (isHost) return;

                        setVLCPauseLock({ time: data.time });

                        return;
                    }
                    case "OK": {
                        if (!isHost) return;

                        //Got host timestamp OK
                        roundTrip.current.push(receivedAt - data.nonceTS);
                        if (roundTrip.current.length > 5) roundTrip.current.shift();

                        return;
                    }
                    case "iamid": {
                        if (!isHost) return;

                        //Set the CF Durable Object ID in Firebase
                        set(ref(database, `/session/${id}/status/conductor`), data.id);
                        wsID = "new/" + data.id;

                        return;
                    }
                    case "error": {
                        console.error("Got an error from the sync server", data);

                        return;
                    }
                    default: {
                        console.warn("Got unknown websocket message", data);

                        return;
                    }
                }
            });

            function handleCloseOrError(evt) {
                //Only handle websocket closing once
                if (ws.hasHandledClose) return;
                ws.hasHandledClose = true;

                wsNumErrors.current++;

                //Double-check that the socket is closed
                ws.close();

                //Capture closing error with Sentry (only on the first failure)
                if ((evt?.type === "error") && (wsNumErrors.current === 1)) {
                    console.log("Capturing WebSocket exception", evt);
                    captureException(evt);
                }

                if (wsShouldBeOpen && (wsNumErrors.current > 3)) {
                    //We already tried to reopen the socket a few times without success
                    //Wait a bit before trying again
                    console.log("WebSocket", wsID, "closed & refusing to re-open, but should be open, waiting a bit before trying again...");
                    setTimeout(() => {
                        if (wsShouldBeOpen) openWebSocket();
                    }, wsNumErrors.current > 10 ? 2000 : 500);
                } else if (wsShouldBeOpen) {
                    //Reopen socket if we still need it
                    console.log("WebSocket", wsID, "closed, but should be open; re-opening...");
                    openWebSocket();
                } else {
                    console.log("WebSocket", wsID, "closed");
                }
            }

            //Listen for closed or errored connection
            ws.addEventListener('close', handleCloseOrError);
            ws.addEventListener('error', handleCloseOrError);

            activeWS.current = ws;
            window.__red_condWS = ws;
        }

        openWebSocket();

        return () => {
            wsShouldBeOpen = false;
            ws.close();
            activeWS.current = null;
            roundTrip.current = [];
            setPlaybackSyncAccuracy(0);
        }
    }, [isHost ? !playbackState : !!playbackState && sessionState?.conductor, playbackRef]);

    //Set pause state
    useEffect(() => {
        if (!vlcPauseLock || !playbackState) return;

        //Set to pause
        if (playbackState?.state !== "paused") {
            window.redNode.pauseVLC();
        }

        //Seek VLC
        if (vlcPauseLock.time !== playbackState.time) {
            window.redNode.seekVLCTo({ time: vlcPauseLock.time });
        }
    }, [vlcPauseLock, playbackState]);

    //Sync host playback
    useEffect(() => {
        if (!isHost || !activeWS.current || (activeWS.current.readyState !== WebSocket.OPEN)) return;

        if (playbackState?.state === "playing") {
            //Send the current playback time to the server
            const offset = getOffset(roundTrip.current);
            activeWS.current.send(JSON.stringify({ type: "hostTime", nonceTS: new Date().getTime(), time: calcCurrentPlaybackTime(playbackState) + offset, adj: true }));
        } else if (playbackState?.state === "paused") {
            //Send the current paused time to the server
            activeWS.current.send(JSON.stringify({ type: "hostPause", time: playbackState?.time }));
        }
    }, [isHost && playbackState?.time, isHost && playbackState?.state]);

    return (
        <playbackAccuracyContext.Provider value={{ playbackAccuracy: playbackSyncAccuracy, pauseLock: vlcPauseLock, clientOffset }}>
            {children}
        </playbackAccuracyContext.Provider>
    );
}