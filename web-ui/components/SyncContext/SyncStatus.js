import React, { createContext, useContext, useState, useEffect } from "react";
import { ref, set, onDisconnect } from "firebase/database";
import { useUser } from "../FirebaseProvider";
import { usePlaybackState } from "../PlaybackContext";
import { fileSyncContext } from "./FileSync";
import { playbackAccuracyContext } from "./PlaybackSync";
import { database } from "../FirebaseProvider";
import { useSessionMembers, useSessionState } from "../SessionContext";

export const syncStatusContext = createContext(null);

export default function SyncStatus({ id, children, isHost, hostIsOnline }) {
    const user = useUser();
    const fileSync = useContext(fileSyncContext);
    const { playbackAccuracy, pauseLock } = useContext(playbackAccuracyContext);
    const playbackState = usePlaybackState();
    const members = useSessionMembers();
    const sessionState = useSessionState();

    const [syncStatus, setSyncStatus] = useState("joining");
    const [description, setDescription] = useState(null);

    //Update status according to current state
    useEffect(() => {
        if (playbackState === false) {
            //VLC not open
            return setSyncStatus("browsing");
        }

        if (isHost) {
            if (playbackState?.information?.meta?.filename && playbackAccuracy) {
                //Host has file open and has an open WebSocket
                return setSyncStatus("controlling");
            } else if (playbackState?.information?.meta?.filename && !playbackAccuracy) {
                //Host has file open but no WebSocket
                return setSyncStatus("hostNoSocket");
            } else {
                //Host does not have a file open
                return setSyncStatus("controllerNoFile");
            }
        } else {
            if (!hostIsOnline) {
                //Client is waiting for host
                return setSyncStatus("waiting");
            } else if (fileSync.isSynced === false) {
                //Client without file
                return setSyncStatus("clientLocatingFile");
            } else if (fileSync.isSynced === null) {
                //Client is waiting for file
                return setSyncStatus("waiting");
            } else if ((fileSync.isSynced === true) && pauseLock) {
                //Client is pause locked
                return setSyncStatus("paused");
            } else if ((fileSync.isSynced === true) && playbackAccuracy) {
                //Client is syncing
                return setSyncStatus("syncing_" + playbackAccuracy);
            } else if ((fileSync.isSynced === true) && !playbackAccuracy) {
                //Client does not have a socket
                return setSyncStatus("clientNoSocket");
            } else {
                //Client is doing nothing
                console.warn("Could not determine client status. This probably shouldn't ever happen.");
                return setSyncStatus("waiting");
            }
        }
    }, [playbackState === false, isHost, playbackState?.information?.meta?.filename, sessionState?.video, fileSync, playbackAccuracy, pauseLock]);

    //Update sync status in Firebase
    useEffect(() => {
        if (!syncStatus || !id || !user?.uid) return;

        //Get the status to report
        let statusToSet = syncStatus;
        //if (syncStatus.includes("syncing_")) statusToSet = "syncing";

        //Database location
        const syncRef = ref(database, `/session/${id}/members/${user.uid}`);

        //Remove sync status on disconnect
        function removeOnDisconnect() {
            const disconnectRef = onDisconnect(syncRef);
            disconnectRef.remove();

            return () => {
                disconnectRef.cancel();
            }
        }

        //Do nothing (but still set disconnect listener)
        if (syncStatus === members?.[user?.uid]) return removeOnDisconnect();

        //Set in firebase
        set(syncRef, statusToSet);

        //Set disconnect listener
        return removeOnDisconnect();
    }, [syncStatus, members?.[user?.uid]]);

    return (
        <syncStatusContext.Provider value={{ status: syncStatus, description }}>
            {children}
        </syncStatusContext.Provider>
    )
}