import React, { useContext } from "react";
import { Icon } from "shamrock-ux";
import { fileSyncContext } from "./SyncContext/FileSync";
import { playbackAccuracyContext } from "./SyncContext/PlaybackSync";

export default function DebuggingStats() {
    const { clientOffset, pauseLock } = useContext(playbackAccuracyContext);
    const fileSync = useContext(fileSyncContext);

    return (
        <div className="flex-1 font-mono space-y-4">
            <div className="flex flex-row items-center space-x-2">
                <Icon className="text-xl">bug_report</Icon>
                <h5 className="text-lg">debugging stats</h5>
            </div>
            <p><b>clientOffset</b> {clientOffset}</p>
            <p><b>pauseLock</b> {JSON.stringify(pauseLock)}</p>
            <p><b>fileSync.isSynced</b> {JSON.stringify(fileSync.isSynced)}</p>
        </div>
    );
}