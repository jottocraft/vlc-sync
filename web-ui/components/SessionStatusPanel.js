import { ref, remove, set } from "firebase/database";
import React, { useCallback, useEffect, useMemo, useState, useContext } from "react";
import { Button, Icon, Link, Modal, Table } from "shamrock-ux";
import DebuggingStats from "./DebuggingStats";
import { database, useUser, useUsernames } from "./FirebaseProvider";
import { usePlaybackState, useVLCState } from "./PlaybackContext";
import { usePreferences } from "./Preferences";
import { useMySessionRole, useSessionContent, useSessionMembers, useSessionPerms, useSessionState } from "./SessionContext";
import { useSyncStatus } from "./SyncContext";
import { fileSyncContext } from "./SyncContext/FileSync";

const PEOPLE_STATUSES = {
    joining: { icon: "person_add", text: "Joining", dotdotdot: true },
    browsing: { icon: "visibility", text: "Browsing" },
    waiting: { icon: "hourglass_empty", text: "Waiting", dotdotdot: true },
    syncing: { icon: "play_circle", text: "Syncing playback", dotdotdot: true },
    paused: { icon: "pause", text: "Playback paused" },
    hostNoSocket: { icon: "nest_remote_comfort_sensor", text: "Connecting", dotdotdot: true },
    clientNoSocket: { icon: "sensors", text: "Connecting", dotdotdot: true },
    controlling: { icon: "settings_remote", text: "Controlling playback", dotdotdot: true },
    controllerNoFile: { icon: "file_open", text: "Open a file" },
    clientLocatingFile: { icon: "manage_search", text: "Can't find file" }
};

export const getStatus = function (id) {
    if (!id) return;

    if (id.startsWith("syncing_")) {
        //Client syncing status
        const syncAccuracy = Number(id.replace("syncing_", ""));

        if (syncAccuracy >= 4) return { color: "text-violet-500", icon: "auto_awesome", text: "Syncing playback", dotdotdot: true };
        if (syncAccuracy >= 3) return { color: "text-green-500", icon: "sync", text: "Syncing playback", dotdotdot: true };
        if (syncAccuracy >= 2) return { color: "text-orange-500", icon: "sync_problem", text: "Syncing playback", dotdotdot: true };
        if (syncAccuracy >= 1) return { color: "text-red-500", icon: "sync_disabled", text: "Syncing playback", dotdotdot: true };
    } else {
        return PEOPLE_STATUSES[id];
    }
}

function useVLCIsOpen() {
    const playback = usePlaybackState();
    return useMemo(() => playback !== false);
}

function useIsHost(uid) {
    const state = useSessionState();
    return useMemo(() => state?.host === uid);
}

export default function SessionStatusPanel({ id }) {
    const members = useSessionMembers();
    const user = useUser();

    const vlcIsOpen = useVLCIsOpen();
    const { vlcIsOpening, openVLC } = useVLCState();
    
    const content = useSessionContent();
    const state = useSessionState();
    
    const perms = useSessionPerms();
    const myRole = useMySessionRole();
    const isHost = useIsHost(user?.uid);

    const { status: syncStatus, myStatusDescription } = useSyncStatus();
    const myStatus = getStatus(syncStatus);
    const { tryAgain } = useContext(fileSyncContext);

    const [dotdotdot, setDotDotDot] = useState("...");
    useEffect(() => {
        let dots = "...";
        const dotInterval = setInterval(() => {
            dots += ".";
            if (dots.length > 3) dots = ".";
            setDotDotDot(dots);
        }, 1000);

        return () => clearInterval(dotInterval);
    }, []);

    const toggleHost = useCallback(() => {
        if (isHost) {
            remove(ref(database, `/session/${id}/status/host`));
        } else {
            set(ref(database, `/session/${id}/status/host`), user?.uid);
        }
    }, [isHost, user?.uid]);

    const uids = useMemo(() => members ? Object.keys(members) : [], [members]);
    const usernames = useUsernames(uids);

    const { showOffset, mediaFolder } = usePreferences();

    return (
        <div className="py-20">
            <div className="text-center">
                <div>
                    <div className="flex flex-row items-center justify-center">
                        <Icon style={{ fontSize: 38 }} className={"mr-5" + (myStatus?.color ? " " + myStatus?.color : "")}>{myStatus?.icon}</Icon>
                        <h1 className="text-type-0 text-5xl font-semibold">{myStatus?.text}{myStatus?.dotdotdot ? <span className="w-10 inline-block text-left">{dotdotdot}</span> : null}</h1>
                    </div>
                    {myStatusDescription ? <h2 className="text-lg mt-6 font-medium text-type-20">{myStatusDescription}</h2> : null}
                </div>
                {syncStatus === "clientLocatingFile" ? (
                    <div className="mt-8">
                        <p className="text-sm text-type-20 mb-2">VLC Sync was unable to locate the session's current file in your media folder. Please verify that the file exists, then try scanning again.</p>
                        {content?.download ? <p className="text-sm text-type-20 mb-2">The administrators of this session have provided a link to download the media they're watching. If you trust them, click <Link target="_blank" href={content?.download}>here</Link> to visit that link.</p> : null}
                        <p><b>Current file</b>: {state?.video}</p>
                        <p><b>Your media folder</b>: {mediaFolder}</p>
                        <br />
                        {syncStatus === "clientLocatingFile" ? <Button size="small" onClick={tryAgain} icon="search">Scan for file again</Button> : null}
                    </div>
                ) : null}
                <br /><br />
                {window.redNode ? (
                    <div className="space-x-4">
                        {!vlcIsOpen && !vlcIsOpening ? <Button onClick={openVLC} style="primary" icon="open_in_new">Open VLC</Button> : null}
                        {perms?.owner === user?.uid || myRole === "admin" || perms?.anyHost ? <Button onClick={toggleHost} icon={isHost ? "sync_disabled" : "settings_remote"}>{isHost ? "Give up control" : "Take Control"}</Button> : null}
                    </div>
                ) : (
                    <p>Please download the VLC Sync desktop application to sync playback with this session</p>
                )}
            </div>
            <br /><br /><br />
            <div className="flex flex-row items-top">
                {showOffset ? <DebuggingStats /> : null}
                <div className="flex-1">
                    <h4 className="text-2xl mb-4 font-mediums">People</h4>
                    {Object.keys(members ?? {}).map(member => {
                        const status = getStatus(members[member]);
                        if (!status) return null;
                        const username = usernames[member];
                        return (
                            <div key={member} className="flex flex-row items-center py-4 space-x-4">
                                <Icon className={`text-xl ${status.color ?? ""}`}>{status.icon}</Icon>
                                <span>{username === null ? "Anonymous" : username ?? member}</span>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    );
}