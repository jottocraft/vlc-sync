import React, { createContext, useCallback, useContext, useState } from "react";
import { ref, set, remove } from "firebase/database";
import { useSetGlobalModal } from "./AppRoot";
import { database, useUser } from "./FirebaseProvider";
import PlaybackContext from "./PlaybackContext";
import SessionContext from "./SessionContext";
import SyncContext from "./SyncContext";

const joinedSessionContext = createContext(null);

export function useJoinedSession() {
    return useContext(joinedSessionContext);
}

export default function JoinedSessionRoot({ children }) {
    const [joinedSession, setJoinedSession] = useState(null);

    //Get user
    const user = useUser();

    //Enter the joining state to get DB permissions
    const setGlobalModal = useSetGlobalModal();
    const joinSession = useCallback((id) => {
        if (!user?.uid || !id) {
            if (joinedSession) {
                //We're existing a session we're in
                //Remove our name from the members list
                console.log("Exiting session", joinedSession, "->", id);
                remove(ref(database, `/session/${joinedSession}/members/${user?.uid}`));
            }
            setJoinedSession(null);
            return;
        }

        if (joinedSession !== id) {
            //Joining session
            console.log("Joining session", joinedSession, "->", id);

            const myMemberRef = ref(database, `/session/${id}/members/${user.uid}`);
            set(myMemberRef, "joining").then(() => {
                setJoinedSession(id);
            }).catch((e) => {
                console.error("Session join error", e);
                setGlobalModal({
                    title: "Couldn't join session",
                    icon: "error",
                    message: "An error occurred when joining the specified session. Check the console for more details."
                });
            });
        }
    }, [user?.uid, joinedSession]);

    return (
        <joinedSessionContext.Provider value={{ id: joinedSession, update: joinSession }}>
            {joinedSession ? (
                <PlaybackContext>
                    <SessionContext id={joinedSession}>
                        <SyncContext id={joinedSession}>
                            {children}
                        </SyncContext>
                    </SessionContext>
                </PlaybackContext>
            ) : children}
        </joinedSessionContext.Provider>
    );
}