import React, { createContext, useContext, useEffect, useState } from "react";
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, onAuthStateChanged } from "firebase/auth";
import { get, getDatabase, ref } from "firebase/database";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA_egYxUggsfTGMgFLqpXEQwiZ6Kq9qmx0",
    authDomain: "jottocraft-red-player.firebaseapp.com",
    databaseURL: "https://jottocraft-red-player.firebaseio.com",
    projectId: "jottocraft-red-player",
    storageBucket: "jottocraft-red-player.appspot.com",
    messagingSenderId: "792787111903",
    appId: "1:792787111903:web:70fe25fcb02e0aa7d33e6a",
    measurementId: "G-780MWX2H1C"
};

//Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const googleProvider = new GoogleAuthProvider();
export const database = getDatabase(app);

const userContext = createContext(undefined);

export function useUser() {
    return useContext(userContext);
}

//Cache Usernames to prevent fetching more than nessasary
const GLOBAL_USERNAME_CACHE = {};

export async function getUserUID(username) {
    return get(ref(database, `/users/uid/${username}`)).then(s => s.val());
}

//A helper function to get usernames
export function useUsernames(uids) {
    const [displayNames, setDisplayNames] = useState(GLOBAL_USERNAME_CACHE);

    useEffect(() => {
        function fetchUsernames() {
            //Fetch only new usernames
            const promises = uids.filter(uid => GLOBAL_USERNAME_CACHE[uid] === undefined).map(function fetchUsername(uid) {
                console.log("Fetching username for user", uid);
                return get(ref(database, `/users/username/${uid}`)).then(snap => {
                    let username = null;
                    if (snap) {
                        username = snap.val();
                    }

                    //Store username result to cache
                    GLOBAL_USERNAME_CACHE[uid] = username ?? null;
                });
            });

            //Set results
            Promise.all(promises).finally(() => {
                setDisplayNames({ ...GLOBAL_USERNAME_CACHE });
            });
        }

        fetchUsernames();

        document.addEventListener("redRefetchUsernames", fetchUsernames);
        return () => document.removeEventListener("redRefetchUsernames", fetchUsernames);
    }, [uids]);

    return displayNames;
}

export function refetchUsername(id) {
    console.log("Force updating username cache for user", id);
    delete GLOBAL_USERNAME_CACHE[id];
    document.dispatchEvent(new CustomEvent("redRefetchUsernames"));
}

export default function FirebaseProvider({ children }) {
    //Listen for user
    const [user, setUser] = useState(undefined);
    useEffect(() => {
        const unsub = onAuthStateChanged(auth, (user) => {
            console.log("Firebase user updated", user);

            if (user) {
                setUser(user);
            } else {
                setUser(null);
            }
        });

        return () => unsub();
    }, [auth]);

    return (
        <userContext.Provider value={user}>
            {children}
        </userContext.Provider>
    )
}