import { off, ref, remove, set, onValue } from "firebase/database";
import { useRouter } from "next/router";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Button, ButtonGroup, Icon, InputBox, Modal, Table } from "shamrock-ux";
import { useSetGlobalModal } from "./AppRoot";
import { database, getUserUID, useUsernames } from "./FirebaseProvider";
import { useSessionPerms } from "./SessionContext";

export default function SessionPermissionsPanel({ id }) {
    const router = useRouter();
    const sessionPerms = useSessionPerms();
    const setGlobalModal = useSetGlobalModal();

    //Import session permissions and roles info into editable states
    const [editingPrivate, setEditingPrivate] = useState(sessionPerms?.private);
    const [editingAnyHost, setEditingAnyHost] = useState(sessionPerms?.anyHost);
    const [editingRoles, setEditingRoles] = useState(null);

    //Listen to active user roles
    const [liveRoles, setLiveRoles] = useState(null);
    useEffect(() => {
        let firstFetch = true;
        const allRolesRef = ref(database, `/session/${id}/roles`);
        onValue(allRolesRef, snap => {
            if (firstFetch) {
                firstFetch = false;
                setEditingRoles({ ...snap.val() });
            }
            setLiveRoles(snap.val());
        });

        return () => off(allRolesRef);
    }, [id]);

    //Filter editor user roles by role
    const editingAdmins = useMemo(() => editingRoles ? Object.keys(editingRoles).filter(u => editingRoles[u] === "admin") : null, [editingRoles]);
    const editingMembers = useMemo(() => editingRoles ? Object.keys(editingRoles).filter(u => editingRoles[u] === "member") : null, [editingRoles]);

    //Get usernames
    const uids = useMemo(() => editingRoles ? Object.keys(editingRoles) : [], [editingRoles]);
    const usernames = useUsernames(uids);

    //Additional states not part of the final session perms
    const [typedUsername, setTypedUsername] = useState("");

    //Reset changes to active values
    const discardChanges = useCallback(() => {
        setEditingPrivate(sessionPerms?.private);
        setEditingAnyHost(sessionPerms?.anyHost);
        setEditingRoles({ ...liveRoles });
    }, [sessionPerms, liveRoles]);

    //Assign a role to a user with the provided username
    const assignUsernameRole = useCallback(async (username, role) => {
        //Clear username box
        setTypedUsername("");

        //Get the UID corresponding to this username
        const uid = await getUserUID(username);

        if (!uid) {
            setGlobalModal({
                title: "User not found",
                icon: "error",
                message: "VLC Sync couldn't find a user named " + username
            });

            return;
        }

        editingRoles[uid] = role;
        setEditingRoles({ ...editingRoles });
    }, [setGlobalModal, editingRoles]);

    //Save editing state into Firebase
    const saveContent = useCallback(() => {
        //Set private state
        if (sessionPerms?.private !== editingPrivate) {
            set(ref(database, `/session/${id}/perms/private`), editingPrivate);
        }

        //Set anyHost state
        if (sessionPerms?.anyHost !== editingAnyHost) {
            set(ref(database, `/session/${id}/perms/anyHost`), editingAnyHost);
        }

        //Save user roles
        Object.keys(editingRoles).forEach(uid => {
            if (editingRoles[uid] !== liveRoles[uid]) {
                //Save updated user role
                if (editingRoles[uid]) {
                    set(ref(database, `/session/${id}/roles/${uid}`), editingRoles[uid]);
                } else {
                    //Delete user role
                    remove(ref(database, `/session/${id}/roles/${uid}`));
                }
            }
        });
    }, [id, sessionPerms, editingRoles, editingPrivate, editingAnyHost, liveRoles]);

    const deleteSession = useCallback(() => {
        remove(ref(database, `/sessionList/${id}`)).then(remove(ref(database, `/session/${id}`))).then(() => {
            router.push("/");
        });
    }, [id, router]);

    return (
        <div className="py-20">
            <div>
                <h1 className="text-type-0 text-4xl font-semibold">Session Permissions</h1>
                <h2 className="mt-4 text-type-20 text-lg">Only you, the creator of this session, has access to these settings</h2>
            </div>
            <br />
            <div className="flex flex-col xl:flex-row mt-12">
                <div className="flex-1 space-y-12 xl:mr-12">
                    <div>
                        <p className="text-type-10 text-lg mb-2">Session access</p>
                        <p className="text-type-30 text-sm mb-4">Making your session public will allow anyone, including anonymous users, to find and access your session</p>
                        <ButtonGroup items={[
                            { label: "Private", icon: "lock", active: editingPrivate, onClick: () => setEditingPrivate(true) },
                            { label: "Public", icon: "public", active: !editingPrivate, onClick: () => setEditingPrivate(false) }
                        ]} />
                    </div>
                    <div>
                        <p className="text-type-10 text-lg mb-2">Playback control</p>
                        <p className="text-type-30 text-sm mb-4">Set restrictions for who can control this session's playback</p>
                        <ButtonGroup items={[
                            { label: "Admins only", icon: "lock", active: !editingAnyHost, onClick: () => setEditingAnyHost(false) },
                            { label: "Any viewer", icon: "lock_open", active: editingAnyHost, onClick: () => setEditingAnyHost(true) }
                        ]} />
                    </div>
                    <div>
                        <p className="text-type-10 text-lg mb-2">Advanced</p>
                        <p className="text-red-500 font-medium text-sm mb-4">Deleting a session cannot be reversed. Proceed with caution.</p>
                        <Button onClick={deleteSession} icon="delete" style="outline">Delete session</Button>
                    </div>
                </div>
                <div className="flex-1 space-y-12 mt-12 xl:mt-0">
                    <div>
                        <p className="text-type-10 text-lg mb-2">Assign roles</p>
                        <p className="text-type-30 text-sm mb-4">Enter a username below and select a role to assign</p>
                        <div className="space-y-2">
                            <InputBox placeholder="Username" value={typedUsername} onChange={e => setTypedUsername(e.target.value)} icon="face" width={500} />
                            <div className="flex flex-row items-center space-x-2">
                                <Button onClick={() => assignUsernameRole(typedUsername, "member")} icon="person" size="small">Make member</Button>
                                <Button onClick={() => assignUsernameRole(typedUsername, "admin")} icon="admin_panel_settings" size="small">Make admin</Button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h4 className="text-type-10 text-2xl mb-2">Admins</h4>
                        <div className="flex flex-row items-center flex-wrap text-type-20">
                            <div className="flex flex-row items-center space-x-2 p-2">
                                <Icon>edit</Icon>
                                <span>Set session content</span>
                            </div>
                            <div className="flex flex-row items-center space-x-2 p-2">
                                <Icon>settings_remote</Icon>
                                <span>Control playback when restricted</span>
                            </div>
                            <div className="flex flex-row items-center space-x-2 p-2">
                                <Icon>list</Icon>
                                <span>All member permissions</span>
                            </div>
                        </div>
                        <div className="w-full h-0.5 my-4 bg-ui-10"></div>
                        <div>
                            {!editingAdmins ? (
                                <p>Loading...</p>
                            ) : !editingAdmins.length ? (
                                <p>There aren't any admins in this session</p>
                            ) : editingAdmins.map(u => (
                                <div className="py-2 flex flex-row items-center justify-between" key={u}>
                                    <p>{usernames[u] ?? u}</p>
                                    <Button onClick={() => { editingRoles[u] = null; setEditingRoles({ ...editingRoles }); }} icon="delete" size="small"></Button>
                                </div>
                            ))}
                        </div>
                    </div>
                    <div>
                        <h4 className="text-type-10 text-2xl mb-2">Members</h4>
                        <div className="flex flex-row items-center flex-wrap text-type-20">
                            <div className="flex flex-row items-center space-x-2 p-2">
                                <Icon>people</Icon>
                                <span>See viewers list</span>
                            </div>
                            <div className="flex flex-row items-center space-x-2 p-2">
                                <Icon>lock</Icon>
                                <span>Join session when private</span>
                            </div>
                        </div>
                        <div className="w-full h-0.5 my-4 bg-ui-10"></div>
                        <div>
                            {!editingMembers ? (
                                <p>Loading...</p>
                            ) : !editingMembers.length ? (
                                <p>There aren't any members in this session</p>
                            ) : editingMembers.map(u => (
                                <div className="py-2 flex flex-row items-center justify-between" key={u}>
                                    <p>{usernames[u] ?? u}</p>
                                    <Button onClick={() => { editingRoles[u] = null; setEditingRoles({ ...editingRoles }); }} icon="delete" size="small"></Button>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
            <div className="mt-20 flex flex-row items-center space-x-2">
                <Button onClick={saveContent} icon="save">Save</Button>
                <Button onClick={discardChanges} style="outline" icon="close">Discard changes</Button>
            </div>
        </div>
    );
}