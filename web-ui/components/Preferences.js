import React, { createContext, useContext, useState, useEffect } from "react";

const preferenceContext = createContext({});

export function usePreferences() {
    return useContext(preferenceContext);
}

export default function PreferencesProvider({ children }) {
    const [vlcExe, setVLCExe] = useState(undefined);
    const [mediaFolder, setMediaFolder] = useState(undefined);

    const [trackSync, setTrackSync] = useState(undefined);
    const [showOffset, setShowOffset] = useState(undefined);
    const [lowLatency, setLowLatency] = useState(undefined);

    //Store changes in storage
    useEffect(() => {
        if ((trackSync === undefined) || (showOffset === undefined) || (lowLatency === undefined)) return;
        window.localStorage.setItem("trackSync", String(trackSync));
        window.localStorage.setItem("showOffset", String(showOffset));
        window.localStorage.setItem("lowLatency", String(lowLatency));
    }, [trackSync, showOffset, lowLatency]);

    useEffect(() => {
        //Handle storage changes
        function updateFromStorage(e) {
            setVLCExe(window.localStorage.getItem("vlcExePath"));
            setMediaFolder(window.localStorage.getItem("videoFolderPath"));
            setTrackSync(window.localStorage.getItem("trackSync") !== "false");
            setShowOffset(window.localStorage.getItem("showOffset") === "true");
            setLowLatency(window.localStorage.getItem("lowLatency") !== "false");
        }

        updateFromStorage();
        document.addEventListener("redNodeStorageUpdate", updateFromStorage);
        return () => document.removeEventListener("redNodeStorageUpdate", updateFromStorage);
    }, []);

    return (
        <preferenceContext.Provider value={{
            vlcExe, mediaFolder, trackSync, showOffset, lowLatency,
            setVLCExe, setMediaFolder, setTrackSync, setShowOffset, setLowLatency
        }}>
            {children}
        </preferenceContext.Provider>
    )
}