import React, { createContext, useContext, useEffect, useState } from "react";
import { ref, onValue, off } from "firebase/database";
import { database, useUser } from "./FirebaseProvider";

const sessionStateContext = createContext(null);
const sessionPermissionContext = createContext(null);
const sessionContentContext = createContext(null);
const sessionMemberContext = createContext(null);
const sessionMyRoleContext = createContext(null);

export const useSessionState = () => useContext(sessionStateContext);
export const useSessionPerms = () => useContext(sessionPermissionContext);
export const useSessionContent = () => useContext(sessionContentContext);
export const useSessionMembers = () => useContext(sessionMemberContext);
export const useMySessionRole = () => useContext(sessionMyRoleContext);

export default function SessionContext({ id, children }) {
    const [state, setState] = useState(null);
    const [perms, setPerms] = useState(null);
    const [content, setContent] = useState(null);
    const [members, setMembers] = useState(null);
    const [myRole, setMyRole] = useState(null);
    const user = useUser();

    //Add listeners
    useEffect(() => {
        if (!user?.uid || !id) return;

        //Get session state
        const stateRef = ref(database, `/session/${id}/status`);
        onValue(stateRef, snap => setState(snap.val()));

        //Get session permissions
        const permRef = ref(database, `/session/${id}/perms`);
        onValue(permRef, snap => setPerms(snap.val()));

        //Get session content
        const contentRef = ref(database, `/session/${id}/info`);
        onValue(contentRef, snap => setContent(snap.val()));

        //Get session members
        const memberRef = ref(database, `/session/${id}/members`);
        onValue(memberRef, snap => {
            setMembers(snap.val());
        });

        //Get my role
        const myRoleRef = ref(database, `/session/${id}/roles/${user?.uid}`);
        onValue(myRoleRef, snap => setMyRole(snap.val()));

        return () => {
            off(stateRef);
            off(permRef);
            off(contentRef);
            off(memberRef);
            off(myRoleRef);
        };
    }, [id, user?.uid]);

    return (
        <sessionStateContext.Provider value={state}>
            <sessionPermissionContext.Provider value={perms}>
                <sessionContentContext.Provider value={content}>
                    <sessionMemberContext.Provider value={members}>
                        <sessionMyRoleContext.Provider value={myRole}>
                            {children}
                        </sessionMyRoleContext.Provider>
                    </sessionMemberContext.Provider>
                </sessionContentContext.Provider>
            </sessionPermissionContext.Provider>
        </sessionStateContext.Provider>
    );
}