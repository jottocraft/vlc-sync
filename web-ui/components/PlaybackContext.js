import React, { createContext, useContext, useEffect, useState, useCallback, useMemo, useRef } from "react";
import { Modal, Button } from "shamrock-ux";
import { useSetGlobalModal } from "./AppRoot";

const playbackContext = createContext(null);
const vlcContext = createContext(null);
const playbackRefContext = createContext(null);

export function usePlaybackState() {
    return useContext(playbackContext);
}

export function usePlaybackRef() {
    return useContext(playbackRefContext);
}

export function useVLCState() {
    return useContext(vlcContext);
}

//Adjusts for latency and time passed since polling
export function calcCurrentPlaybackTime(playback) {
    if (!playback?.at) return playback?.time;
    return ((new Date().getTime() - playback.at) / 1000) + playback.time;
}

export default function PlaybackContext({ children }) {
    const [vlcIsOpening, setVLCIsOpening] = useState(false);
    const [promptModuleInstall, setPromptModuleInstall] = useState(false);
    const setGlobalModal = useSetGlobalModal();
    const openVLC = useCallback(() => {
        setVLCIsOpening(true);

        window.redNode.openVLC().then(() => {
            setVLCIsOpening(false);
        }).catch(e => {
            switch (e) {
                case "cannot-find-vlc": {
                    console.log("Cannot find VLC");
                    setGlobalModal({
                        title: "Cannot find VLC",
                        icon: "error",
                        message: "VLC Sync cannot find the location of your VLC installation. If you don't have VLC installed yet, please download and install it from VideoLAN.org, then try again. If you do have VLC installed, please set your VLC location at Settings -> Set VLC location, then try again."
                    });
                    setVLCIsOpening(false);
                    return;
                }
                case "vlc-module-install": {
                    console.log("VLC needs module install");
                    setPromptModuleInstall("new");
                    return;
                }
                case "vlc-module-update": {
                    console.log("VLC needs module update");
                    setPromptModuleInstall("update");
                    return;
                }
                default: {
                    console.error("Error opening VLC", e);
                    setVLCIsOpening(false);
                    return;
                }
            }
        });
    }, []);

    const installModule = useCallback(() => {
        setPromptModuleInstall(false);
        window.redNode.installModule().then(openVLC).catch(e => {
            console.error("Error installing module", e);
            setVLCIsOpening(false);
        });
    }, [openVLC]);

    //Track VLC playback
    const [playback, setPlayback] = useState(false);
    const playbackRef = useRef(false);

    useEffect(() => {
        if (!window.redNode) return;

        const unsub = window.redNode.pollVLC(data => {
            window.__red_playbackState = data;
            setPlayback(data);
            playbackRef.current = data;
        });

        return () => {
            setPlayback(false);
            playbackRef.current = false;
            unsub();
        };
    }, []);

    //Don't send playback state if VLC is still opening (however, keep polling to make playback available instantly when it's ready)
    const playbackState = vlcIsOpening ?  false : playback;

    return (
        <>
            {promptModuleInstall ? (
                <Modal onClose={() => { setPromptModuleInstall(false); setVLCIsOpening(false); }} icon="extension" title={`${promptModuleInstall === "new" ? "Install" : "Update"} the VLC Sync integration module`}>
                    <div className="mb-2">
                        {promptModuleInstall === "new" ? (
                            <p>VLC Sync requires installing an integration module into VLC in order to sync playback. The module will only run when you use VLC through VLC Sync.</p>
                        ) : (
                            <p>A new version of the VLC Sync integration module is available.</p>
                        )}
                    </div>
                    <p>Please provide administrator permissions when prompted to allow the module files to be copied into your VLC installation.</p>
                    <br />
                    <Button onClick={installModule} icon="security" style="primary">Install module</Button>
                </Modal>
            ) : null}
            <vlcContext.Provider value={{ vlcIsOpening, openVLC }}>
                <playbackContext.Provider value={playbackState}>
                    <playbackRefContext.Provider value={playbackRef}>
                        {children}
                    </playbackRefContext.Provider>
                </playbackContext.Provider>
            </vlcContext.Provider>
        </>
    )
}