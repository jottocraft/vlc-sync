const Sentry = require("@sentry/electron/renderer");

Sentry.init({
    dsn: "https://dbe2f6d1e5b4493d9a2653f536a10ffc@o226991.ingest.sentry.io/6585654",
    release: process.env.release,
    dist: process.env.dist
});