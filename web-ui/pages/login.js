import React, { useState, useCallback, useMemo, useEffect } from "react";
import { Button, InputBox, Link, Modal } from "shamrock-ux";

import { createUserWithEmailAndPassword, sendPasswordResetEmail, signInWithEmailAndPassword, signInWithRedirect, getRedirectResult, signInAnonymously } from "firebase/auth";

import { auth, database, googleProvider } from "../components/FirebaseProvider";
import { ref, set } from "firebase/database";

getRedirectResult(auth).then(RR => {
    console.log("Firebase redirect result", RR);
}).catch(error => {
    console.error("Firebase auth error", error);
});

export default function Login() {
    const [isCreatingAccount, setIsCreatingAccount] = useState(false);

    const [err, setErr] = useState(null);
    const [msg, setMsg] = useState(null);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const catchFirebaseError = useCallback(e => setErr(e.message));

    const resetPassword = useCallback(() => {
        if (!email) return setErr("Please enter your account's email address in the email field, then try again");
        sendPasswordResetEmail(auth, email).then(() => {
            setMsg("Follow the instructions sent to " + email + " to reset your password.");
        }).catch(catchFirebaseError);
    }, [email, catchFirebaseError]);

    const signIn = useCallback(() => {
        if (!email || !password) return setErr("Please enter an email and password");
        signInWithEmailAndPassword(auth, email, password).then(cred => {
            console.log("Signed in", cred);
        }).catch(catchFirebaseError);
    }, [email, password, catchFirebaseError]);

    const createAccount = useCallback(() => {
        if (password !== confirmPassword) {
            setMsg("Your passwords do not match. Please try again.");
            return;
        }

        createUserWithEmailAndPassword(auth, email, password).then(cred => {
            console.log("Signed in", cred);
        }).catch(catchFirebaseError);
    }, [email, password, confirmPassword, catchFirebaseError]);

    const googleSignIn = useCallback(() => {
        signInWithRedirect(auth, googleProvider).catch(catchFirebaseError);
    }, [catchFirebaseError]);

    const anonymousSignIn = useCallback(() => {
        signInAnonymously(auth).catch(catchFirebaseError);
    }, [catchFirebaseError]);

    const formSubmit = useMemo(() => {
        return isCreatingAccount ? createAccount : signIn;
    }, [isCreatingAccount, createAccount, signIn]);

    const [isWebUI, setIsWebUI] = useState(false);
    useEffect(() => setIsWebUI(!window.redNode), []);

    return (
        <div className="w-full pt-32 flex flex-col items-center justify-center">
            {err ? (
                <Modal icon="error" title="Error" onClose={() => setErr(null)}>
                    <p>{err}</p>
                </Modal>
            ) : msg ? (
                <Modal icon="notifications" title="Alert" onClose={() => setMsg(null)}>
                    <p>{msg}</p>
                </Modal>
            ) : null}
            <div className="w-full max-w-lg flex-grow flex flex-col items-center justify-center">
                <div className="space-y-6 p-10 rounded-xl shadow-lg w-full bg-ui-0 dark:bg-ui-5">
                    <div className="flex flex-col">
                        <div className="flex flex-row items-center">
                            <img className="w-12 h-12 mr-4" src="/app/icon.svg" />
                            <h1 className="font-semibold text-2xl">VLC Sync</h1>
                        </div>
                        <h2 className="mt-8 text-lg">{isCreatingAccount ? "Create an account" : "Sign in to continue"}</h2>
                        {isWebUI ? (
                            <p className="mt-2 text-sm text-type-30">You're using the web version of VLC Sync with limited feature availability. If you'd like to sync playback (Windows only), please download the full desktop app <Link href="https://red-server.jottocraft.com/update/latest/exe">here</Link>.</p>
                        ) : null}
                    </div>
                    <div className="space-y-2">
                        <InputBox onKeyDown={e => (e.key == "Enter") && formSubmit()} value={email} onChange={(e) => setEmail(e.target.value)} icon="email" type="email" placeholder="Email address" />
                        <InputBox onKeyDown={e => (e.key == "Enter") && formSubmit()} value={password} onChange={(e) => setPassword(e.target.value)} icon="password" type="password" placeholder="Password" />
                        {isCreatingAccount ? (
                            <InputBox onKeyDown={e => (e.key == "Enter") && formSubmit()} value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} icon="password" type="password" placeholder="Confirm password" />
                        ) : null}
                    </div>
                    <div className="flex flex-row items-center space-x-4">
                        {isCreatingAccount ? (
                            <>
                                <Button onClick={() => setIsCreatingAccount(false)} icon="arrow_back" style="outline">Back</Button>
                                <Button onClick={formSubmit} icon="person_add" style="primary">Create account</Button>

                            </>
                        ) : (
                            <>
                                <Button onClick={formSubmit} icon="login" style="primary">Sign in</Button>
                                <Button onClick={() => setIsCreatingAccount(true)} icon="person_add">Create account</Button>

                            </>
                        )}
                    </div>
                    {isCreatingAccount ? null : <Button onClick={resetPassword} size="small" icon="refresh" style="outline">Reset password</Button>}
                </div>
                <div className="space-y-6 w-full max-w-lg p-8 rounded-xl shadow-lg mt-10 bg-ui-0 dark:bg-ui-5">
                    <div className="space-x-4">
                        <Button onClick={googleSignIn} size="small">Sign in with Google</Button>
                        <Button onClick={anonymousSignIn} size="small" style="outline">Continue anonymously</Button>
                    </div>
                </div>
            </div>
            <div className="flex-shrink">
                <div className="py-14">
                    <div className="text-center text-type-20">
                        <div className="flex flex-row items-center justify-center">
                            <img alt="jottocraft logo" className="h-10 mr-4"
                                src="https://cdn.jottocraft.com/images/footerImage.png" />
                            <h5 className="text-2xl font-medium">jottocraft</h5>
                        </div>
                        <p className="mt-2">(c) jottocraft 2020-2023. This project is <Link target="_blank" href="https://jottocraft.com/oss">open source</Link>.</p>
                    </div>
                </div>
            </div>
        </div>
    );
}