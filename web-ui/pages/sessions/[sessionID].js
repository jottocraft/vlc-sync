import React, { useCallback, useEffect, useState } from "react";
import { Button, Icon, Sidebar } from "shamrock-ux";
import { ref, child, get, set, remove } from "firebase/database";
import { database, useUser } from "../../components/FirebaseProvider";
import { useRouter } from 'next/router';
import SessionStatusPanel, { getStatus } from "../../components/SessionStatusPanel";
import SessionContentPanel from "../../components/SessionContentPanel";
import SessionPermissionsPanel from "../../components/SessionPermissionsPanel";
import { useMySessionRole, useSessionContent, useSessionPerms } from "../../components/SessionContext";
import { useSyncStatus } from "../../components/SyncContext";
import { useJoinedSession } from "../../components/JoinedSessionRoot";

function SessionPage({ id }) {
    const route = useRouter();
    const [view, setView] = useState("status");
    const content = useSessionContent();
    const perms = useSessionPerms();
    const myRole = useMySessionRole();
    const user = useUser();

    const { status: syncStatus, description } = useSyncStatus();
    const myStatus = getStatus(syncStatus);

    const joinedSession = useJoinedSession();
    const exitSession = useCallback(() => {
        route.push("/sessions").then(() => {
            joinedSession.update(null);
        });
    }, [id, user?.uid]);

    return (
        <Sidebar
            header={content?.name ? {
                title: content.name,
                icon: perms.private ? "lock" : "public"
            } : undefined}
            items={[
                { name: myStatus?.text ?? "Playback", size: "big", icon: myStatus?.icon ?? "sync", active: view === "status", onClick: () => setView("status") },
                { type: "spacer" },
                { visible: perms?.owner === user?.uid || myRole === "admin", name: "Content Settings", icon: "description", active: view === "content", onClick: () => setView("content") },
                { visible: perms?.owner === user?.uid, name: "Permissions", icon: "rule", active: view === "permissions", onClick: () => setView("permissions") },
                { type: "spacer" },
                { name: "Exit", icon: "logout", onClick: exitSession }
            ].filter(i => i.visible !== false)}>
            {view === "status" ? (
                <SessionStatusPanel id={id} />
            ) : view === "content" ? (
                <SessionContentPanel id={id} />
            ) : view === "permissions" ? (
                <SessionPermissionsPanel id={id} />
            ) : (
                <p>404 Not Found</p>
            )}
        </Sidebar>
    );
}

export default function SessionPreJoin() {
    //Get session ID
    const route = useRouter();
    const id = route.query.sessionID;

    //Join session on ID change
    const joinedSession = useJoinedSession();
    useEffect(() => {
        joinedSession.update(id);
    }, [id]);

    if (joinedSession?.id === id) return <SessionPage id={id} />;
    return <p className="text-3xl text-center py-40">Joining session...</p>;
}