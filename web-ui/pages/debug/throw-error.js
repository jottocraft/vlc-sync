import React, { useEffect } from "react";

export default function ThrowError() {
    useEffect(() => {
        throw new Error("jottocraft_thrown_error_for_debugging");
    }, []);

    return (
        <p>Hello world</p>
    );
}