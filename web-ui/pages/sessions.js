import React, { useCallback, useEffect, useState } from "react";
import { Button, Container, Icon, InputBox, Modal } from "shamrock-ux";
import { ref, child, get, set, push } from "firebase/database";
import { database, useUser } from "../components/FirebaseProvider";
import Link from "next/link";
import { useSetGlobalModal } from "../components/AppRoot";

export default function Sessions() {
    const user = useUser();
    const [sessionList, setSessionList] = useState(null);
    const fetchSessions = useCallback(() => {
        setSessionList(null);

        get(ref(database, "/sessionList")).then(snap => {
            const sessionIDs = Object.keys(snap.val());

            const sessionInfoPromises = [];
            const sessionData = [];

            //Get session info for each session
            sessionIDs.forEach(id => {
                sessionInfoPromises.push(new Promise((resolve, reject) => {
                    let sessionInfo;
                    get(ref(database, "/session/" + id + "/info")).then(snap => {
                        sessionInfo = snap.val();
                        return get(ref(database, "/session/" + id + "/viewers"));
                    }).then((snap) => {
                        var viewers = snap.val();
                        if (sessionInfo) {
                            sessionData.push({ info: sessionInfo, id, viewers })
                        }
                        resolve();
                    }).catch(e => {
                        resolve();
                    });
                }))
            })

            //Get info for all listed sessions
            Promise.all(sessionInfoPromises).then(() => {
                setSessionList(sessionData);
            });
        });
    }, []);

    useEffect(() => {
        fetchSessions();
    }, []);


    const [newSessionName, setNewSessionName] = useState(false);
    const createSession = useCallback(async () => {
        const name = newSessionName;
        console.log("creating session", newSessionName);

        //Create the session in Firebase
        const newSession = push(ref(database, `/session`), {
            info: {
                name: newSessionName
            },
            perms: {
                owner: user.uid,
                anyHost: false,
                private: true
            }
        });

        //Add the session ID to the global session ID list
        const newSessionList = set(ref(database, `/sessionList/${newSession.key}`), newSession.key);

        //Wait for both to finish
        await Promise.all([ newSession, newSessionList ]);

        //Refresh session list and close dialog
        fetchSessions();
        setNewSessionName(false);
    }, [newSessionName, user, fetchSessions]);

    const setGlobalModal = useSetGlobalModal();
    const openCreateSession = useCallback(() => {
        if (!user || user?.isAnonymous) {
            setGlobalModal({
                title: "Cannot create session",
                icon: "error",
                message: "You must sign in with a registered account in order to create a session"
            });
            
            return;
        }

        setNewSessionName("");
    }, [user?.isAnonymous]);

    return (
        <div className="py-20">
            {newSessionName !== false ? (
                <Modal onClose={() => setNewSessionName(false)} title="Create session" icon="add">
                    <p className="text-type-30">All sessions are private by default</p>
                    <br />
                    <p className="mb-2 font-medium">Session name</p>
                    <InputBox value={newSessionName} onChange={e => setNewSessionName(e.target.value)} placeholder="Session name" icon="text_fields" width={500} />
                    <br />
                    <Button onClick={createSession} icon="add">Create session</Button>
                </Modal>
            ) : null}
            <Container>
                <h1 className="text-type-0 text-4xl font-semibold">Sessions</h1>
                <br />
                <div className="space-x-4">
                    <Button onClick={openCreateSession} icon="add">Create a session</Button>
                    <Button onClick={fetchSessions} style="outline" icon="refresh">Refresh</Button>
                </div>
                <br />
                {sessionList ? sessionList.map((s) => {
                    return (
                        <Link key={s.id} href={`/sessions/${s.id}`}>
                            <div className="p-6 hover:bg-ui-5 cursor-pointer rounded">
                                <h3 className="text-2xl font-medium">{s.info.name}</h3>
                                <div className="mt-2 text-type-30 flex flex-row items-center space-x-4">
                                    <div className="flex flex-row items-center space-x-1">
                                        <Icon className="text-lg">visibility</Icon>
                                        <span>{s.viewers ?? 0} viewers</span>
                                    </div>
                                    {s.info.content ? (
                                        <div className="flex flex-row items-center space-x-1">
                                            <Icon className="text-lg">movie</Icon>
                                            <span>{s.info.content}</span>
                                        </div>
                                    ) : null}
                                    {s.info.scheduledStart ? (
                                        <div className="flex flex-row items-center space-x-1">
                                            <Icon className="text-lg">schedule</Icon>
                                            <span>{new Date(s.info.scheduledStart).toLocaleString()}</span>
                                        </div>
                                    ) : null}
                                </div>
                            </div>
                        </Link>
                    )
                }) : <p>Loading...</p>}
            </Container>
        </div>
    );
}