import '../styles/globals.css';
import { Root, Theme } from 'shamrock-ux';
import Head from 'next/head';
import AppRoot from '../components/AppRoot';
import FirebaseProvider from '../components/FirebaseProvider';
import JoinedSessionRoot from '../components/JoinedSessionRoot';
import PreferencesProvider from '../components/Preferences';

function VLCSync({ ...rootProps }) {
  return (
    <>
      <Head>
        <title>VLC Sync</title>
        <meta name="description" content="Sync VLC Playback between devices" />
        <link rel="icon" href="/app/favicon.ico" />
      </Head>
      <Root head={Head}>
        <Theme
          jottocraft
          styles={{
            dark: {
              accent: {
                element: {
                  ui: "#f08428",
                  type: "#3e0314"
                },
                type: "#e27b1e"
              }
            },
            light: {
              accent: {
                element: {
                  ui: "#8f4d19",
                  type: "#ededed"
                },
                type: "#a85800"
              }
            }
          }}>
          <FirebaseProvider>
            <PreferencesProvider>
              <JoinedSessionRoot>
                <AppRoot {...rootProps} />
              </JoinedSessionRoot>
            </PreferencesProvider>
          </FirebaseProvider>
        </Theme>
      </Root>
    </>
  );
}

export default VLCSync;