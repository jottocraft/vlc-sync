import Document, { Html, Head, Main, NextScript } from 'next/document';
import FirebaseProvider from '../components/FirebaseProvider';

class VLCSyncDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    <link rel="preconnect" href="https://fonts.googleapis.com" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default VLCSyncDocument;