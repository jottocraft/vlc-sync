import { ref, set } from "firebase/database";
import React, { useCallback, useState } from "react";
import { Button, InputBox } from "shamrock-ux";
import { useSetGlobalModal } from "../components/AppRoot";
import { database, refetchUsername, useUser } from "../components/FirebaseProvider";

export default function Admin() {
    const [newUsername, setNewUsername] = useState("");
    
    const user = useUser();

    const setGlobalModal = useSetGlobalModal();
    const formSubmit = useCallback(() => {
        if (!newUsername || (newUsername.length > 20) || !/^[A-Za-z0-9_-]*$/.test(newUsername) || newUsername.includes("jottocraft")) {
            setGlobalModal({
                title: "Invalid username",
                icon: "error",
                message: "The username you've entered is either invalid or already taken. Please try again. Usernames can only include letters, numbers, underscores, and dashes. Usernames also cannot exceed 20 characters or include any reserved words."
            });
        }

        Promise.all([
            set(ref(database, `/users/uid/${newUsername}`), user?.uid),
            set(ref(database, `/users/username/${user?.uid}`), newUsername)
        ]).then(() => {
            console.log("Set username", newUsername);
            refetchUsername(user?.uid);
        });
    }, [newUsername, user]);

    return (
        <div className="w-full pt-32 flex flex-col items-center justify-center">
            <div className="w-full max-w-lg flex-grow flex flex-col items-center justify-center">
                <div className="space-y-6 p-10 rounded-xl shadow-lg w-full bg-ui-0 dark:bg-ui-5">
                    <div className="flex flex-col">
                        <div className="flex flex-row items-center">
                            <img className="w-12 h-12 mr-4" src="/app/icon.svg" />
                            <h1 className="font-semibold text-2xl">VLC Sync</h1>
                        </div>
                        <h2 className="mt-8 text-lg">Choose a username</h2>
                        <h4 className="mt-2 text-type-30">Your username will be displayed in sessions you're in and will allow others to add you to private sessions.</h4>
                    </div>
                    <div className="space-y-2">
                        <InputBox onKeyDown={e => (e.key == "Enter") && formSubmit()} value={newUsername} onChange={(e) => setNewUsername(e.target.value)} icon="face" type="text" placeholder="Username" />
                    </div>
                    <div className="flex flex-row items-center space-x-4">
                        <Button onClick={formSubmit} icon="save" style="primary">Save</Button>
                    </div>
                </div>
            </div>
        </div>
    );
}