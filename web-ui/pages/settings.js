import React, { useState, useEffect } from "react";
import { Button, ButtonGroup, Container, ThemeSelector, useTheme } from "shamrock-ux";
import { usePreferences } from "../components/Preferences";

export default function Settings() {
    const theme = useTheme();
    const {
        vlcExe, mediaFolder, trackSync, showOffset, lowLatency,
        setTrackSync, setShowOffset, setLowLatency
    } = usePreferences();

    const appVer = window?.redNode?.appVersion?.();

    return (
        <Container>
            <div className="py-20">
                <div>
                    <h1 className="text-type-0 text-4xl font-semibold">Settings</h1>
                </div>
                <br />
                <p className="text-sm font-mono space-x-8">
                    {appVer ? <span>App version: {appVer}</span> : null}
                    <span>UI version: {process.env.dist} {appVer !== process.env.release ? <>(Built for app version {process.env.release})</> : null}</span>
                </p>
                <br />
                <ThemeSelector />
                <br /><br /><br />
                <div>
                    <h5 className="font-medium text-2xl">File Locations</h5>
                    <p className="mt-2 text-type-10">Set the location of your VLC installation and your media files below:</p>
                    <br />
                    {window.redNode ? (
                        <>
                            <div className="space-y-10">
                                <div className="space-y-2">
                                    <p className="text-type-30">Your VLC.exe location is currently set to: {vlcExe ?? ""}</p>
                                    <Button onClick={window.redNode.vlcFilePicker} icon="file_open" size="small">Set VLC location</Button>
                                </div>
                                <div className="space-y-2">
                                    <p className="text-type-30">Your media folder is currently set to: {mediaFolder ?? ""}</p>
                                    <Button onClick={window.redNode.mediaFolderPicker} icon="video_library" size="small">Set media folder</Button>
                                </div>
                            </div>
                        </>
                    ) : (
                        <p className="text-sm">This setting is only available in the VLC sync app</p>
                    )}
                </div>
                <br /><br /><br />
                <div>
                    <h5 className="font-medium text-2xl">Synchronization</h5>
                    <br />
                    {window.redNode ? (
                        <>
                            <div className="space-y-10">
                                <div>
                                    <p className="text-type-10 text-lg mb-2">Low sync latency</p>
                                    <p className="text-type-30 text-sm mb-4">Disable this if only if you're experiencing constant skipping during playback</p>
                                    <ButtonGroup items={[
                                        { label: "Enable", icon: "flash_on", active: lowLatency, onClick: () => setLowLatency(true) },
                                        { label: "Disable", icon: "flash_off", active: !lowLatency, onClick: () => setLowLatency(false) }
                                    ]} />
                                </div>
                                <div>
                                    <p className="text-type-10 text-lg mb-2">Sync subtitle, audio, and video tracks</p>
                                    <p className="text-type-30 text-sm mb-4">Disable this if you'd like to use different subtitles or audio than the controller of the session</p>
                                    <ButtonGroup items={[
                                        { label: "Sync", icon: "sync", active: trackSync, onClick: () => setTrackSync(true) },
                                        { label: "Don't sync", icon: "sync_disabled", active: !trackSync, onClick: () => setTrackSync(false) }
                                    ]} />
                                </div>
                                <div>
                                    <p className="text-type-10 text-lg mb-2">Show debugging stats</p>
                                    <p className="text-type-30 text-sm mb-4">Enable this to show your sync offset and additional debugging information beyond the status icon</p>
                                    <ButtonGroup items={[
                                        { label: "Show", icon: "visibility", active: showOffset, onClick: () => setShowOffset(true) },
                                        { label: "Hide", icon: "visibility_off", active: !showOffset, onClick: () => setShowOffset(false) }
                                    ]} />
                                </div>
                            </div>
                        </>
                    ) : (
                        <p className="text-sm">This setting is only available in the VLC sync app</p>
                    )}
                </div>
            </div>
        </Container>
    );
}