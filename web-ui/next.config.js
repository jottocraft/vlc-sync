const path = require("path");
const fs = require("fs");
const { withSentryConfig } = require("@sentry/nextjs");

//Get release (app version)
const release = JSON.parse(fs.readFileSync(path.join(__dirname, "..", "app", "package.json"))).version;

//Get dist (web-ui version)
const dist = JSON.parse(fs.readFileSync(path.join(__dirname, "package.json"))).version;

/** @type {import('next').NextConfig} */
const nextConfig = {
  optimizeFonts: false, //This breaks some Shamrock UX animated icons
  reactStrictMode: true,
  productionBrowserSourceMaps: true,
  basePath: '/app',
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.resolve.alias.react = path.join(__dirname, "node_modules", "react");
    config.resolve.alias["react-dom"] = path.join(__dirname, "node_modules", "react-dom");

    // Important: return the modified config
    return config
  },
  env: {
    release,
    dist
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/sessions',
        permanent: true,
      },
    ]
  }
};

if (process?.env?.NODE_ENV === "development") {
  //If in development, export without Sentry SDK
  module.exports = nextConfig;
} else {
  module.exports = withSentryConfig(nextConfig, {
    release,
    dist
  });
}