const functions = require("firebase-functions");

function handleChange(snapshot, context) {
    //Grab the current value of what was written to the Realtime Database
    return snapshot.ref.parent.once("value").then(snap => {
        const data = snap.val();

        //Get the amount of viewers in the session
        const viewers = data ? Object.keys(data).length : 0;

        if (viewers) {
            //Set view count in Firebase
            return snap.ref.parent.child('viewers').set(viewers);
        } else {
            //Remove view count from Firebase
            return snap.ref.parent.child('viewers').remove();
        }
    });
}

exports.handleMemberCreate = functions.database.ref('/session/{sessionID}/members/{uid}').onCreate(handleChange);
exports.handleMemberDelete = functions.database.ref('/session/{sessionID}/members/{uid}').onDelete(handleChange);