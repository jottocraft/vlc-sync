![VLC Sync logo](https://i.imgur.com/gyPa0mr.png)

VLC Sync lets you sync media playback on the VLC media player between devices. With VLC Sync, you can watch movies or listen to music with friends from anywhere in real-time.

VLC Sync is built with Electron, Next.JS, and Cloudflare. The app available exclusively for Windows devices at [vlc-sync.jottocraft.com](https://vlc-sync.jottocraft.com). 

A web version / demo is available at [vlc-sync.jottocraft.com/app](https://vlc-sync.jottocraft.com/app) and can be used to manage your account and sessions, however, the app is required to sync VLC playback.

![A screenshot of VLC Sync syncing playback](https://i.imgur.com/kP8NbFr.png)

## Overview

VLC Sync is comprised of 4 main components:

* An Electron app (/app)
    * The Electron app uses Node.JS APIs to execute all interactions with VLC
* A Next.JS site (/web-ui)
    * The Next.JS site renders the entire UI and does all of the syncing logic. The site is hosted on Cloudflare Pages.
* A Cloudflare Worker (/server)
    * The Cloudflare Worker handles playback syncing communications between devices using WebSockets on Cloudflare Durable Objects, and also serves as the Squirrel update server for the Electron App using Cloudflare KV.
* A Firebase functions project (/functions)
    * Firebase functions are used to run database tasks, such as adjusting the view count for sessions when their member list changes

VLC Sync's main dependencies include Firebase for authentication and database storage, GitLab for repository hosting, CI/CD, and artifact storage, and Jira for issue and development tracking.

## How sync works

The goal of the new VLC Sync design is to cut down on any sources of potential added latency. In the previous version of VLC Sync, a client would need to sync both VLC host playback and a local clock in order to accurately sync playback. This introduced many problems because it depended on collecting host playback from a single point in time. If the sample collected was inaccurate, every connected client would experience inaccurate syncing, which made a "Force Re-sync" button a necessity.

Now, with Cloudflare, VLC Sync is able to efficiently and continuously stream the host's playback state. This new design allows for latency to be accounted through the entire communications stack instead of purely relying on client-side calculations.

![An overview of the VLC sync design](https://i.imgur.com/DxtZpnr.png)

## Developer guide

Follow these steps to get VLC Sync running in a local development environment:

1. Run `npm install` in the `app`, `server`, and `web-ui` folders to install all of the Node.JS dependencies needed to run VLC Sync.
2. `cd` into `web-ui`, then run `npm run dev` to start the Next.JS project.
3. `cd` into `app`, then run `npm start` to start the Electron app. (Note: The system title bar is used instead of the app-integrated title bar in dev mode as the window controls conflict with Electron's developer tools).