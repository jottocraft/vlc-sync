/**
 * VLC Sync Realtime Server
 * This Cloudflare Worker provides real-time, low-latency time syncing between VLC Sync clients.
 */

import handleUpdates from "./updates";

//Calculate the offset from an array of round trip latency measurements
function getOffset(roundTripMeasurements) {
  if (!roundTripMeasurements.length) return 0;
  const latency = roundTripMeasurements.reduce((a, b) => a + b) / roundTripMeasurements.length / 2 / 1000;
  console.log("host offset", latency);
  return latency;
}

export default {
  async fetch(request, env) {
    //Parse request URL
    const url = new URL(request.url);

    //Handle squirrel updates
    if (url.pathname.startsWith("/update")) return handleUpdates(url, request, env);

    //Handle POSTing a new conductor
    if ((url.pathname === "/conductor/new") && (request.method === "GET")) {
      //Create a new conductor DO
      //The ID here is safe against random guessing
      const id = env.conductor.newUniqueId();

      //Get conductor object
      const conductorObject = env.conductor.get(id);

      //Connect to socket with write access
      return conductorObject.fetch(new URL("https://red-server.jottocraft.com/do/writeable"), request);
    }

    //Handle GETing an existing conductor
    if (/^\/conductor\/.+?\/$/.test(url.pathname) && (request.method === "GET")) {
      //Get the conductor ID
      const idString = url.pathname.match(/^\/conductor\/(.+?)\/$/)[1];
      let id;
      try {
        id = env.conductor.idFromString(idString);
      } catch (e) {
        return new Response("400 Provided conductor ID is badly formatted", { status: 400 });
      }

      //Get conductor object
      let conductorObject;
      try {
        conductorObject = env.conductor.get(id);
      } catch (e) {
        return new Response("404 Provided conductor ID does not exist", { status: 404 });
      }

      //Connect to socket with write access
      return conductorObject.fetch(new URL("https://red-server.jottocraft.com/do/readonly"), request);
    }

    return new Response("400 Request does not match a valid path + method combination", { status: 400 });
  },
};

/**
 * A conductor for syncing timestamps between multiple VLC Sync clients
 * 
 * For maximum performance and simplicity, this system works without checking Firebase authentication:
 * Anybody can create a new sync session, which returns a key to allow others to connect. The key is stored in secure
 * Firebase storage to allow other clients to connect read-only, while only the client that created the session can write.
 * 
 * If the host changes/disconnects, a new WebSocket SyncSession is created and updated in Firebase.
 * If somebody is removed from the secure Firebase group, they retain access to the sync session until the host
 * changes, however, this is not considered a security risk since the sync session only includes timestamps. The filename
 * and all other potentially sensitive information remains secure in Firebase.
 */
export class SyncConductor {
  constructor(controller, env) {
    this.storage = controller.storage;
    this.id = controller.id.toString();
    this.env = env;
    this.connections = [];
  }

  async fetch(request) {
    let url = new URL(request.url);

    //Get connection properties
    const isWriteable = url.pathname === "/do/writeable";

    //Must connect with a WebSocket
    if (request.headers.get("Upgrade") != "websocket") {
      return new Response("Expected websocket", { status: 400 });
    }

    //Create controller socket pair
    let pair = new WebSocketPair();

    //Handle pair[1]
    await this.handleConductor(pair[1], isWriteable);

    //Return other pair to client
    return new Response(null, { status: 101, webSocket: pair[0] });
  }

  /**
   * Handle conductor WebSocket connection
   * @param {WebSocket} webSocket The WebSocket for communicating with the client
   * @param {boolean} isHost True if the user is the host (controller with write access) of the session
   */
  async handleConductor(webSocket, isHost) {
    //Accept WebSocket
    webSocket.accept();

    //Create our connection and add it to the connections list.
    const connection = { webSocket, isHost, roundTrip: [] };
    this.connections.push(connection);

    //Listen to messages
    webSocket.addEventListener("message", async msg => {
      const receivedAt = new Date().getTime();

      try {
        //Get timestamp data
        const data = JSON.parse(msg.data);

        switch (data.type) {
          case "hostTime": {
            if (!connection.isHost) return;

            //Host playing

            //We don't actually know if it's OK yet but want to get a message ASAP for low latency
            webSocket.send(JSON.stringify({ type: "OK", nonceTS: data.nonceTS }));

            //Broadcast time to clients ASAP
            const offset = getOffset(connection.roundTrip);
            this.broadcast(JSON.stringify({ type: "clientTime", nonceTS: new Date().getTime(), time: data.time + offset, adj: true }));

            return;
          }
          case "OK": {
            //Got client timestamp OK
            connection.roundTrip.push(receivedAt - data.nonceTS);
            if (connection.roundTrip.length > 5) connection.roundTrip.shift();

            return;
          }
          case "hostPause": {
            if (!connection.isHost) return;

            //Host paused
            this.broadcast(JSON.stringify({ type: "clientPause", time: data.time }));

            return;
          }
          case "whoami": {
            webSocket.send(JSON.stringify({ type: "iamid", id: this.id }));

            return;
          }
          default: {
            webSocket.send(JSON.stringify({ type: "error", error: "unknown_message_type" }));

            return;
          }
        }
      } catch (err) {
        //Send errors to the client
        webSocket.send(JSON.stringify({ error: err.stack }));
      }
    });

    //Remove socket connection on close or error
    let closeOrErrorHandler = evt => {
      this.connections = this.connections.filter(c => c !== connection);
      webSocket.close();
    };
    webSocket.addEventListener("close", closeOrErrorHandler);
    webSocket.addEventListener("error", closeOrErrorHandler);
  }

  //Broadcast timestamp updates to all clients
  broadcast(message) {
    //Send message to all connections
    this.connections = this.connections.filter(connection => {
      //Skip sending timestamp updates to host
      if (connection.isHost) return true;

      try {
        //Send message to socket
        connection.webSocket.send(message);
        return true;
      } catch (err) {
        //Remove dead connections
        return false;
      }
    });
  }
}