//Serve updates from KV

export default async function handleUpdates(url, request, env) {
    const releasesMatch = url.pathname.match(/\/update\/win32\/(.*?)\/RELEASES\/?$/);
    if (releasesMatch) {
        //Temporarily pause auto updates from 2.x.x
        if (releasesMatch[1].startsWith("2.")) {
            return new Response(null, { status: 204 });
        }

        //Get main releases file
        const RELEASES = await env.RELEASES.get("RELEASES");

        if (!RELEASES) {
            //Return 204 (empty OK response) if there aren't any releases
            return new Response(null, { status: 204 });
        }

        //Send as octet stream response
        return new Response(RELEASES, {
            status: 200,
            headers: {
                "Content-Type": "application/octet-stream"
            }
        });
    }

    //Redirect to the latest exe for new installations
    if (url.pathname === "/update/latest/exe") {
        //Get main releases file
        const RELEASES = await env.RELEASES.get("RELEASES");

        //Get version from release
        const VERSION = RELEASES.match(/jottocraft-red-(.*?)-full\.nupkg/)[1];

        if (!VERSION) {
            return new Response("Failed to parse the latest VLC Sync version", { status: 500 })
        }

        //Redirect to latest exe artifact
        return new Response(null, {
            status: 307,
            headers: {
                "Location": `https://cdn.jottocraft.com/artifacts/red/VLC_Sync-${VERSION}_Setup.exe`
            }
        });
    }

    return new Response("404 Not Found", { status: 404 });
}