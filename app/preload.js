const { contextBridge, ipcRenderer } = require('electron');
const fs = require("fs");
const fg = require('fast-glob');
const escapeGlob = require('glob-escape');
const path = require("path");
const { execFile, exec } = require("child_process");

require('@sentry/electron/preload');
const Sentry = require('@sentry/electron/renderer');
if (!ipcRenderer.sendSync("is-dev")) {
  Sentry.init({ dsn: "https://dbe2f6d1e5b4493d9a2653f536a10ffc@o226991.ingest.sentry.io/6585654" });
}

//Red debug
window.__red_debug = {};

//Save changes and broadcast to React
function setAndBroadcastStorage(name, value) {
    window.localStorage.setItem(name, value);
    document.dispatchEvent(new CustomEvent("redNodeStorageUpdate", { detail: name }));
}

//Set default paths
if (!window.localStorage.getItem("videoFolderPath")) setAndBroadcastStorage("videoFolderPath", ipcRenderer.sendSync("default-videos"));
if (!window.localStorage.getItem("vlcExePath")) setAndBroadcastStorage("vlcExePath", "C:\\Program Files\\VideoLAN\\VLC\\vlc.exe");

//Node fetch
const nFetch = require("node-fetch");

const LATEST_MODULE_VER = 106;
const VLC_API_HEADERS = {
    Authorization: "Basic " + btoa(":anime")
};

//Expose APIs for interacting with the local PC & VLC
contextBridge.exposeInMainWorld("redNode", {
    openVLC,
    installModule,
    pollVLC,
    vlcFilePicker,
    mediaFolderPicker,
    seekVLCTo,
    playVLC,
    pauseVLC,
    stopVLC,
    setVLCTrack,
    openVLCFile,
    titleBarTheme,
    appVersion: () => ipcRenderer.sendSync("app-version")
});

function setVLCTrack(type, val) {
    if (!["video", "audio", "spu"].includes(type)) return;
    nFetch("http://localhost:9090/requests/jottocraft.json?command=" + type + "_track&val=" + val, { headers: VLC_API_HEADERS });
}

//Set the theme of the title bar controls
function titleBarTheme({ color, symbolColor, height }) {
    return ipcRenderer.sendSync("title-bar-theme", { color, symbolColor, height });
}

//Find and open the file in VLC
async function openVLCFile(name) {
    //Get video folder
    const videoFolderPath = window.localStorage.getItem("videoFolderPath");

    //Scan using fast-glob
    const glob = `**/${escapeGlob(name)}`;
    console.log("[preload] fg using glob expression", glob, "in", videoFolderPath);
    const matches = await fg([glob], { cwd: videoFolderPath });
    console.log("[preload] fg file matches", matches);

    //Get the file match
    const match = matches[0];
    if (!match) return false;
    const file = path.join(videoFolderPath, match);

    //Open the file in VLC
    console.log("[preload] using media file", file);
    nFetch("http://localhost:9090/requests/jottocraft.json?command=playitem&input=" + file.replace("/", "\\"), { headers: VLC_API_HEADERS });

    return true;
}

//Open a file picker for choosing where VLC is
function vlcFilePicker() {
    ipcRenderer.invoke("set-vlc-exe").then((result) => {
        if (result) setAndBroadcastStorage("vlcExePath", result);
    });
}

//Open the folder picker to set a media folder location
function mediaFolderPicker() {
    ipcRenderer.invoke("set-media-folder").then((result) => {
        if (result) setAndBroadcastStorage("videoFolderPath", result);
    });
}

//POLL VLC ----------------------
let vlcLatency;
function pollVLC(cb) {
    const pollInterval = setInterval(() => {
        const before = new Date().getTime();
        nFetch("http://localhost:9090/requests/jottocraft.json", { headers: VLC_API_HEADERS }).then(r => r.json()).then(data => {
            const after = new Date().getTime();

            //Approximate latency
            vlcLatency = (after - before) / 2;
            data.at = after - vlcLatency;

            cb(data);
        }).catch(e => {
            cb(false);
        });
    }, 500);

    return () => clearInterval(pollInterval);
}

//SEEK VLC TO THE PROVIDED TIME ---------
function seekVLCTo({ time, at }) {
    //Use adjusted time only if the at parameter is provided
    let usingTime = time;
    if (at) usingTime = time + ((new Date().getTime() - at + vlcLatency) / 1000);
    return nFetch("http://localhost:9090/requests/jottocraft.json?command=directseek&val=" + usingTime, { headers: VLC_API_HEADERS });
}

//PLAY VLC FROM THE PROVIDED TIME ---------
function playVLC() {
    return nFetch("http://localhost:9090/requests/jottocraft.json?command=play", { headers: VLC_API_HEADERS });
}

//PAUSE VLC AT THE PROVIDED TIME ---------
function pauseVLC() {
    return nFetch("http://localhost:9090/requests/jottocraft.json?command=pause", { headers: VLC_API_HEADERS });
}

//STOP VLC (CLOSE FILE) ---------
function stopVLC() {
    return nFetch("http://localhost:9090/requests/jottocraft.json?command=stop", { headers: VLC_API_HEADERS });
}

//OPEN VLC ----------------------
let vlcProcess, usingExePath;
function openVLC() {
    return new Promise((fResolve, fReject) => {
        //The EXE path we are deciding to use
        usingExePath = window.localStorage.getItem("vlcExePath");
        new Promise((resolve, reject) => {
            //Check the VLC exe path
            if (fs.existsSync(usingExePath)) {
                resolve();
            } else if (fs.existsSync('C:\\Program Files\\VideoLAN\\VLC\\vlc.exe')) {
                usingExePath = 'C:\\Program Files\\VideoLAN\\VLC\\vlc.exe';
                resolve();
            } else if (fs.existsSync('C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe')) {
                usingExePath = 'C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe';
                resolve();
            } else {
                reject("cannot-find-vlc");
            }
        }).then(() => {
            //Check if VLC is already open, if not, open it
            return new Promise((resolve, reject) => {
                vlcProcess = execFile(usingExePath, ['--extraintf=http', '--http-port=9090', '--http-password=anime']);
                resolve();
            });
        }).then((data) => {
            //Wait for VLC to open. If it is already open (deprecated), skip this step
            return new Promise((resolve, reject) => {
                if (data) {
                    resolve(data);
                } else {
                    var resolved = false;
                    var checkVLC = setInterval(() => {
                        nFetch("http://localhost:9090/requests/jottocraft.json", { headers: VLC_API_HEADERS }).then(r => {
                            if (r.status === 404) {
                                clearInterval(checkVLC);
                                resolved = true;
                                reject("vlc-module-install");
                            } else {
                                clearInterval(checkVLC);
                                resolved = true;
                                r.json().then(data => {
                                    resolve(data);
                                });
                            }
                        });
                    }, 1000);
                }
            });
        }).then((data) => {
            //Check for module updates
            if (data.jottocraft >= LATEST_MODULE_VER) {
                fResolve();
            } else {
                fReject("vlc-module-update");
            }
        }).catch(e => {
            //Pass error back to web UI for further action
            fReject(e);
        });
    })
}

function installModule() {
    const ROOT_FOLDER = ipcRenderer.sendSync("root-folder");
    console.log("[preload.js]", "Using ROOT_FOLDER at", ROOT_FOLDER);
    const logPath = path.join(ROOT_FOLDER, "resources", "vlc-lua-module", "moduleinstaller.log");
    if (fs.existsSync(logPath)) fs.unlinkSync(logPath);
    return new Promise((resolve, reject) => {
        nFetch("http://localhost:9090/requests/jottocraft.json?command=quit", { headers: VLC_API_HEADERS }).then(r => {
            if (r.status === 404) throw "e404";
            next();
        }).catch(function (e) {
            if (e === "e404") {
                if (vlcProcess && vlcProcess.kill && vlcProcess.kill()) {
                    next();
                } else {
                    let res = false;
                    window.alert("Please close the VLC window so that the integration module can be installed");
                    let interval = setInterval(() => {
                        nFetch("http://localhost:9090/requests/status.json", { headers: VLC_API_HEADERS }).catch(function (e) {
                            if (!res) {
                                res = true;
                                clearInterval(interval);
                                next();
                            }
                        })
                    }, 1000);
                }
            } else {
                next();
            }
        });

        function next() {
            console.log("[preload.js]", "Installing module...");
            let res = false;
            let interval = setInterval(() => {
                if (fs.existsSync(logPath) && !res) {
                    res = true;
                    clearInterval(interval);
                    fs.unlinkSync(logPath);
                    console.log("[preload.js]", "Module install complete");
                    resolve();
                }
            }, 1000);
            exec(`"${path.join(ROOT_FOLDER, "resources", "vlc-lua-module", "copymodule.bat").replace("/", "\\")}" "${path.join(usingExePath, "..").replace("/", "\\")}"`);
        }
    });
}