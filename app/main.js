const { app, BrowserWindow, autoUpdater, dialog, ipcMain, shell, session } = require('electron');
const path = require("path");
const fs = require("fs");
const isDev = require('electron-is-dev');

const Sentry = require("@sentry/electron/main");
if (!isDev) {
  Sentry.init({ dsn: "https://dbe2f6d1e5b4493d9a2653f536a10ffc@o226991.ingest.sentry.io/6585654" });
}

//Prevent app from running on setup
if (require('electron-squirrel-startup')) return;

// Force Single Instance Application
const shouldRun = app.requestSingleInstanceLock();
if (!shouldRun) {
  app.quit();
  return;
}

const server = "https://red-server.jottocraft.com"
const feed = `${server}/update/${process.platform}/${app.getVersion()}`

autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
  const dialogOpts = {
    type: 'info',
    buttons: ['Install'],
    title: 'Application Update',
    detail: 'A new version has been downloaded. VLC Sync will now restart to apply the updates.'
  }

  dialog.showMessageBox(dialogOpts).then((returnValue) => {
    autoUpdater.quitAndInstall();
  });
});

autoUpdater.on('error', message => {
  console.error('UPDATE ERROR', message);
});

//Check for updates if in production

if (!isDev) {
  autoUpdater.setFeedURL(feed);
  autoUpdater.checkForUpdates();

  setInterval(() => {
    autoUpdater.checkForUpdates();
  }, 600000);
}

//Disable media key support
app.commandLine.appendSwitch('disable-features', 'HardwareMediaKeyHandling,MediaSessionService');

let win;
async function createWindow() {
  //Load React devtools if available
  const REACT_DEVTOOLS = path.join(process.env.LOCALAPPDATA, "Google\\Chrome\\User Data\\Default\\Extensions", "fmkadmapgofadopljbjfkapdkoienihi");
  console.log("Checking for react devtools at", REACT_DEVTOOLS);
  if (fs.existsSync(REACT_DEVTOOLS)) {
    console.log("Loading react devtools...");
    const DEVTOOLS_VERSION = String(fs.readdirSync(REACT_DEVTOOLS)[0]);
    console.log("Using devtools version", DEVTOOLS_VERSION);
    await session.defaultSession.loadExtension(path.join(REACT_DEVTOOLS, DEVTOOLS_VERSION));
  }

  // Create the browser window.
  console.log("Creating VLC Sync window");
  win = new BrowserWindow({
    width: 800,
    height: 600,
    icon: path.join(__dirname, "icon.ico"),
    title: "VLC Sync",
    autoHideMenuBar: true,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    },
    titleBarOverlay: !isDev,
    titleBarStyle: isDev ? "default" : "hidden"
  })

  // and load the index.html of the app.
  win.loadFile('index.html')
  win.loadURL(isDev ? 'http://localhost:3000/app' : 'https://vlc-sync.jottocraft.com/app');
  win.maximize();

  win.webContents.on('did-finish-load', () => {
    // Protocol handler for win32
    if (process.platform == 'win32') {
      win.webContents.send('deepLinkArgs', process.argv.slice(1))
    }
  });

  win.webContents.on('new-window', function (e, url) {
    e.preventDefault();
    shell.openExternal(url);
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

app.on('second-instance', (event, argv, cwd) => {
  // Someone tried to run a second instance, we should focus our window.
  if (win) {
    if (win.isMinimized()) win.restore();
    win.focus();

    if (process.platform == 'win32') {
      win.webContents.send('deepLinkArgs', argv.slice(1));
    }
  }
});

//Handle jottocraft-red URL
app.setAsDefaultProtocolClient('jottocraft-red');

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

//Allow the app to change the title bar color (only on production since it messes with devtools)
ipcMain.on("title-bar-theme", (event, arg) => {
  if (!isDev) {
    event.returnValue = win.setTitleBarOverlay({
      color: arg.color,
      symbolColor: arg.symbolColor,
      height: arg.height
    });
  } else {
    event.returnValue = false;
  }
});

//Expose isDev
ipcMain.on("is-dev", (event, arg) => {
  event.returnValue = isDev;
});

//Expose the default videos folder to the preload script
ipcMain.on("default-videos", (event, arg) => {
  event.returnValue = app.getPath("videos");
});

//Expose the app version
ipcMain.on("app-version", (event, arg) => {
  event.returnValue = app.getVersion();
});

//The app root path
ipcMain.on("root-folder", (event, arg) => {
  const appPath = app.getAppPath();
  event.returnValue = isDev ? appPath : path.join(appPath, "..", "..");
});

ipcMain.handle("set-vlc-exe", async (event, someArgument) => {
  const results = await dialog.showOpenDialog({
    title: "Set VLC.exe location",
    buttonLabel: "Set VLC.exe location",
    properties: ['openFile'],
    filters: [{ name: 'EXE File', extensions: ['exe'] }]
  });

  if (!results.canceled && results.filePaths[0]) {
    return results.filePaths[0];
  }

  return false;
});

ipcMain.handle("set-media-folder", async (event, someArgument) => {
  const results = await dialog.showOpenDialog({
    title: "Set VLC Sync media folder location",
    buttonLabel: "Set media folder",
    properties: ['openDirectory']
  });

  if (!results.canceled && results.filePaths[0]) {
    return results.filePaths[0];
  }

  return false;
});